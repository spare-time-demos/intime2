package org.manathome.intime2.api

import io.micrometer.core.annotation.Timed
import io.micrometer.core.instrument.Metrics
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import mu.KLogging
import org.manathome.intime2.trace.GoogleAnalyticsForwarder
import org.manathome.intime2.trace.TrackData
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

/**
 *  logging, tracing and tracking endpoint for remote client (spa).
 *
 *  @author man@home
 */
@RestController
@Api
@CrossOrigin(maxAge = 3600,methods = [RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS])
class TraceController {

    companion object : KLogging()

    // tag::prometheus-counter[]
    private val metricLogCalls = Metrics.counter("trace.log.calls")
    // end::prometheus-counter[]

    private val gaForwarder = GoogleAnalyticsForwarder()

    /** log sink for client side logging
     *  should forward client side logging to server side log.
     *
     *  will be used by jsnlog ajaxappender on react client.
     */
    @ApiOperation(
            value = "log remote client exceptions",
            nickname = "log"
    )
    @Timed
    @PostMapping("/trace/log", consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    fun log(@RequestBody log: String,
            request: HttpServletRequest) : ResponseEntity<String> {

        // TODO: mrj, handle all kinds of log attacs on open endpoint? too many, too large, forged...
        // TODO: mrj, client id or sessions id, enabling matching client and server side logs.
        // TODO: mrj, parse array of log messages and log seperately with correct log level.

        try {
            val clientId = request.getHeader("JSNLog-RequestId")

            var body = "received"
            if (log.length > 15000) {
                body = "ignored."
                logger("org.manathome.intime2.trace.CLIENTLOG").info { log.substring(0, 15000) }
            } else {
                logger("org.manathome.intime2.trace.CLIENTLOG").info { "$clientId / $log" }
            }
            // tag::prometheus-increment[]
            metricLogCalls.increment()
            // end::prometheus-increment[]
            return ResponseEntity(body, HttpStatus.OK)
        }catch(ex: Exception) {
            logger.error("logging error", ex, log);
            return ResponseEntity("ignored!", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    /**
     * sink for client side tracking data, should forward to tracking provider/analytics.
     *
     * dummy implementation so far.
     *
     * TODO: client timestamp, visit-id, visitor-id....
     */
    @ApiOperation(
            value = "usage tracking remote client",
            nickname = "track"
    )
    @Timed
    @PostMapping("/trace/track", consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    fun track(@RequestBody tracks: Array<TrackData>?,
              @RequestHeader(value="User-Agent") userAgent: String?,
              request: HttpServletRequest?) : ResponseEntity<String>
    {
        if(tracks != null && tracks.isNotEmpty()) {

            logger.debug { "received tracking data ${tracks.size} entries from ${request?.remoteAddr}" }

            try {

                val count = gaForwarder.send(
                        tracks,
                        userAgent ?: "unknown",
                        "TODO:4711",                // TODO: mrj clientId (userid) - what to use and where to keep (cookie, header...)
                        request?.remoteAddr
                )
                return ResponseEntity("$count", HttpStatus.OK)
            }catch(ex: Exception) {
                logger.warn { "tracking data not logged, exception $ex"}
                return ResponseEntity("0", HttpStatus.INTERNAL_SERVER_ERROR)
            }
        }
        else {
            logger.warn { "empty track received." }
            return ResponseEntity("0", HttpStatus.OK)
        }
    }

}
