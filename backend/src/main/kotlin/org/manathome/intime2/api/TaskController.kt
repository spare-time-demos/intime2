package org.manathome.intime2.api

import io.micrometer.core.annotation.Timed
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import mu.KLogging
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid
import javax.validation.constraints.Positive

/**
 *   task api (used on task admin pages)
 *   @author a man@home
 */
@Timed
@RestController
@Api
@CrossOrigin(maxAge = 3600,methods = [RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS])
class TaskController(
        private val taskRepository: TaskRepository,
        private val workItemRepository: WorkItemRepository
) {

    companion object : KLogging()

    @ApiOperation(
            value = "retrieving all known tasks.",
            nickname = "task"
    )
    @GetMapping("/task", produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun findTasks(): Array<Task> {
        return taskRepository
                .findAll()
                .toTypedArray()
    }
    @ApiOperation(
            value = "save new or changed task.",
            nickname = "saveTask"
    )
    @PostMapping(
            "/saveTask",
            consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE ]
    )
    fun save(
            @RequestBody
            @Valid
            task: Task
    ): Task {

        logger.info { "saving task ${task}" }

        return taskRepository
                .save(task);
    }

    @ApiOperation(
            value = "delete task with given id.",
            notes = "delete will only succeed if no work was already done on given task.",
            nickname = "deleteTask"
    )
    @PostMapping(
            "/deleteTask",
            consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE ]
    )
    fun delete(
            @RequestBody
            @Valid
            @Positive
            taskId: Long
            ): Long {

        logger.info { "delete task $taskId" }

        val wis = workItemRepository.findByTaskId(taskId);
        if(wis.count() > 0) {
            logger.info { "delete task $taskId rejected, contains work." }
            throw DataOperationNotAllowedException("task ${taskId} cannot be deleted, contains work.")
        }
        else
        {
            taskRepository.deleteById(taskId)
            return taskId
        }
    }
}
