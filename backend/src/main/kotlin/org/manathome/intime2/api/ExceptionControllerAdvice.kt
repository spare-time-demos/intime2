package org.manathome.intime2.api

import mu.KLogging
import org.springframework.beans.TypeMismatchException
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.context.request.WebRequest
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import javax.validation.ConstraintViolationException

/** configure global error handling for api rest services.
 *  @author man@home
 * */
@Suppress("unused")
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
class ExceptionControllerAdvice : ResponseEntityExceptionHandler() {

    companion object : KLogging()

    override fun handleTypeMismatch(ex: TypeMismatchException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        logger.error("returning type mismatch ${ex.message}")
        return super.handleTypeMismatch(ex, headers, status, request)
    }

    @ExceptionHandler(DataNotFoundException::class)
    protected fun handleConflict(ex: DataNotFoundException, request: WebRequest): ResponseEntity<Any> {
        logger.error("returning data not found: ${ex.message}")
        return handleExceptionInternal(ex, ex.message, HttpHeaders(), ex.status, request)
    }

    @ExceptionHandler(DataOperationNotAllowedException::class)
    protected fun handleConflict(ex: DataOperationNotAllowedException, request: WebRequest): ResponseEntity<Any> {
        logger.error("update or delete rejected: ${ex.message}")
        return handleExceptionInternal(ex, ex.message, HttpHeaders(), ex.status, request)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    protected fun handleConflict(ex: ConstraintViolationException, request: WebRequest): ResponseEntity<Any> {
        logger.error("validation error: ${ex.message}")
        return handleExceptionInternal(ex, ex.message, HttpHeaders(), HttpStatus.BAD_REQUEST, request)
    }


    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected fun handlBadRequeste(ex: Exception) {
        logger.warn("Returning HTTP 400 Bad Request", ex)
    }

    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        logger.warn("Returning HTTP 400 Not Readable ${request.contextPath}", ex)
        return buildErrorResponseEntity("error parsing request. not readable: ${ex.message}", status)
    }


    private fun buildErrorResponseEntity(msg: String, status: HttpStatus): ResponseEntity<Any> {
        return ResponseEntity(msg, status)
    }

}
