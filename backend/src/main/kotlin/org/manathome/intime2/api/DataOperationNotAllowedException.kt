package org.manathome.intime2.api

import org.springframework.http.HttpStatus

/** delete or update not allowed for this object/state. */
class DataOperationNotAllowedException(message: String) : Exception(message) {
    val status: HttpStatus = HttpStatus.BAD_REQUEST
}
