package org.manathome.intime2.api

import org.springframework.http.HttpStatus

/** http not found exception for rest services. */
class DataNotFoundException(message: String) : Exception(message) {
    val status: HttpStatus = HttpStatus.NOT_FOUND
}
