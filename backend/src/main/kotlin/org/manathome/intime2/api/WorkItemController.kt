package org.manathome.intime2.api

import io.micrometer.core.annotation.Timed
import io.micrometer.core.instrument.Metrics
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import mu.KLogging
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.manathome.intime2.pd.WorkItem
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.LocalDate
import javax.validation.Valid

/**
 *   base api entry point for inTime2 frontend.
 *   @author man@home
 */
@Timed
@RestController
@Api
@CrossOrigin(maxAge = 3600,methods = [RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS])
class WorkItemController(
    private val workItemRepository: WorkItemRepository,
    private val taskRepository: TaskRepository
) {

    companion object : KLogging()

    private val metricTestInitData = Metrics.counter("test_init_data")

    @ApiOperation(
            value = "retrieving all work for given task.",
            nickname = "work-task"
    )
    @GetMapping("/task/{taskId}/work", produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun findWorkByTask(
            @ApiParam("task id for work to search", required = true)
            @PathVariable
            taskId: Long): Array<WorkItem> {

        return workItemRepository.findByTaskId(taskId)
                                 .toList()
                                 .toTypedArray();
    }

    /** database read: all work in given week. */
    @ApiOperation(
            value ="database read: all work in given week..",
            nickname = "work-week",
            response = WorkItem::class,
            responseContainer = "List")
        @GetMapping("/work/week/{date}", produces = [ MediaType.APPLICATION_JSON_VALUE ])
    @Timed(value = "rest_work_week_by_date", histogram = false, description = "manfreds description", percentiles = [0.1, 0.5, 1.0])
    fun retrieveWorkForWeek(
        @ApiParam("first day of requested week", required = true, example = "2018-01-15")
        @PathVariable
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        date: LocalDate
    ): Array<WorkItem> {
        logger.info { "getting work for week starting $date" }
        val from: LocalDate = date
        val until = from.plusDays(7)

//        initTestData(from, until)

        val wi = workItemRepository
                .findWorkInTimeRange(from, until)
                .toList()
                .toTypedArray()

        logger.info { "returning ${wi.count()} workItems." }

        return wi
    }

    /** database read: all tasks suitable for given week. */
    @ApiOperation(
            value = "database read: all tasks suitable for given week.",
            nickname = "tasks-week",
            response = Task::class,
            responseContainer = "List")
    @GetMapping("/task/week/{date}", produces = [ MediaType.APPLICATION_JSON_VALUE ])
    fun retrieveTasksForWeek(
        @ApiParam("first day of requested week (local: monday)", required = true, example = "2018-01-15")
        @PathVariable
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
        date: LocalDate
    ): Array<Task> {
        logger.info { "requested tasks $date" }

        val until = date.plusDays(7)

        val tasks = taskRepository.findActiveTasks(date, until).toList()
        logger.info { "returning ${tasks.count()} tasks." }
        return tasks
                .toTypedArray()
    }

    /** database save one week of work, returning sum of saved hours. */
    @ApiOperation(
            value = "database save one week of work, returning sum of saved hours.",
            nickname = "save-work"
    )
    @PostMapping("/work", consumes = [MediaType.APPLICATION_JSON_UTF8_VALUE])
    fun save(@RequestBody @Valid workItems: Array<WorkItem>): Int {

        logger.info { "saving ${workItems.size} work items" }
        workItems.forEach { logger.info { "saving $it" } }

        val savedItems = workItemRepository
                .saveAll(workItems.toList())

        savedItems.forEach { logger.info { "saved $it" } }

        return savedItems
                .map{ x -> x.hours.toInt() }
                .sum()
    }

    /** create test data, debug only. */
    fun initTestData(from: LocalDate, until: LocalDate): Boolean {

        val wic = workItemRepository.findWorkInTimeRange(from, until).count()

        if (0 == wic) {
            logger.info { "creating new test work items for $from" }
            metricTestInitData.increment()

            var t1 = taskRepository.findByName("test task 1").firstOrNull()
            if (t1 == null) {
                t1 = taskRepository.saveAndFlush(Task(-1, "test task 1"))
                logger.info({ "   created new test task -1 $from $t1" })
            }
            workItemRepository.saveAndFlush(WorkItem(-1, from.plusDays(1), BigDecimal("5"), t1))
            workItemRepository.saveAndFlush(WorkItem(-2, from.plusDays(2), BigDecimal("2"), t1))
            workItemRepository.saveAndFlush(WorkItem(-3, from.plusDays(4), BigDecimal("1"), t1))

            var t2 = taskRepository.findByName("test task 2").firstOrNull()
            if (t2 == null) {
                t2 = taskRepository.saveAndFlush(Task(-2, "test task 2"))
                logger.info({ "   created new test task -2 $from $t2" })
            }
            workItemRepository.saveAndFlush(WorkItem(-4, from.plusDays(1), BigDecimal("4"), t2))
            return true
        }
        return false
    }
}
