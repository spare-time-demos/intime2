package org.manathome.intime2.trace

import io.micrometer.core.instrument.Metrics
import mu.KLogging
import java.net.URLEncoder

/**
 *  forward tracking data to google analytics.
 *
 *   see <a href="https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide">
 *        https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide</a>
 *
 *  production url: "https://www.google-analytics.com/collect"
 *  debug url.....: "https://www.google-analytics.com/debug/collect"
 *
 *  @constructor switch between production (no url and debug)
 *
 *  @author a man@home
 */
class GoogleAnalyticsForwarder(val googleAnalyticsUrl: String = "https://www.google-analytics.com/collect" ) {

    companion object : KLogging() {
        val googleAnalyticsDebugUrl: String = "https://www.google-analytics.com/debug/collect"
    }

    private val trackingId = "UA-118343426-2" // todo(man@home): externalize

    private val metricHttpOutboundGa = Metrics.timer("http.outbound.ga")

    /**
     * send tracking data (http post)
     *
     * @param tracks    tracking data (pageviews, events)
     * @param userAgent user agent (retrieved from client browser)
     * @param clientId  user identifier
     * @param clientIp  user ip address (will be anonymized)
     */
    fun send(tracks: Array<TrackData>, userAgent: String, clientId: String, clientIp: String?): Int {

        var sentCount = 0

        if (tracks.isNotEmpty()) {
            tracks.forEach {

                val cid = URLEncoder.encode(clientId, "UTF-8")
                val ip = URLEncoder.encode(clientIp, "UTF-8")
                val pageId = URLEncoder.encode(it.page, "UTF-8")
                val event = if( it.action == null ) "" else URLEncoder.encode(it.action, "UTF-8")
                val eventLabel = if (it.action == null || it.data == null) ""
                                        else URLEncoder.encode(it.data.take(250), "UTF-8")

                val eventType = if (it.action == null) "pageview" else "event"

                // tag::ga-forwarding[]

                val googleAnalyticsTrackData = if (it.action == null)
                    "v=1&tid=$trackingId&cid=$cid&uip=$ip&t=$eventType&dp=$pageId"
                else
                    "v=1&tid=$trackingId&cid=$cid&uip=$ip&t=$eventType&dp=$pageId&ec=ui&ea=$event&el=$eventLabel"


                val response = metricHttpOutboundGa.recordCallable( {
                    khttp.post(
                            url = googleAnalyticsUrl,
                            headers = mapOf("User-Agent" to userAgent),         // forward actual user agent
                            data = googleAnalyticsTrackData,                    // payload
                            timeout = 2.0                                       // do not wait too long for this
                    )
                })

                // end::ga-forwarding[]

                if (response.statusCode != 200) {
                    logger.warn { "tracking ga data returned nok: ${response.statusCode} ${response.text} for $googleAnalyticsTrackData" }
                } else {
                    sentCount++
                    logger.debug { "forwarded to analytics(${response.statusCode}): $googleAnalyticsTrackData " }
                }

                // extra code for validation service: used on unit tests/validations.
                if(googleAnalyticsUrl == googleAnalyticsDebugUrl) {
                    logger.info { "ga validation response: ${response.text} " }

                    if(response.text.contains("ERROR"))
                        throw IllegalArgumentException("GA Validation failed: ${response.text}")
                }
            }
        }

        return sentCount
    }

}
