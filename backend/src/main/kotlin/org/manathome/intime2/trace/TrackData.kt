package org.manathome.intime2.trace

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

/**
 * tracking data, will be forwarded to analytics provider (like google analytics etc.)
 * source: mainly react client with react-tracking library
 * example data: { page: "TimeEntry", component: "TimeRangeSelection", action: "dateChange", parameter: Date 2018-05-06T22:00:00.000Z }
 * @author man@home
 */
@ApiModel(value = "TrackData", description = "client tracking info")
data class TrackData(
        @ApiModelProperty("page", required = true, notes = "page")
        val page: String = "",
        @ApiModelProperty("component", notes = "(react) component")
        val component: String? = "",
        @ApiModelProperty("action", notes = "optional specific action (event) or user interaction")
        val action: String? = null,
        @ApiModelProperty("data", notes = "optional event or page data")
        val data: String? = null
        )
{

}

