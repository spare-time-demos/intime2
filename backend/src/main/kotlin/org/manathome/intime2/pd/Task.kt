package org.manathome.intime2.pd

import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import mu.KLogging
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.Validation
import javax.validation.ValidationException
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

const val TASK_ID_UNDEFINED = -1L

/**
 *  a task that work can be done on.
 *  @author man@home
 */
@ApiModel(value = "Task", description = "each work has to be done on exactly one given task.")
// tag::jpa[]
@Entity
@Table(name = "it_task") // <1>
data class Task(

    @ApiModelProperty("unique primary key", notes = "null only on new task", example="12")
    @Id      // <2>
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = TASK_ID_UNDEFINED,

    @Column(length = 255)
    @get:Size(min = 2, max = 255)
    @get:NotBlank
    @get:NotNull
    @ApiModelProperty(
            "task description",
            required = true,
            notes = "task name prose",
            example="my sample task for work"
    )
    val name: String,

// end::jpa[]

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @ApiModelProperty("optional start date of task", notes = "date only, no time part", example = "2018-01-18")
    @Column(name = "active_from")
    var activeFrom: LocalDate? = null,

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @ApiModelProperty("optional start date of task", notes = "date only, no time part", example="2018-02-20")
    @Column(name = "active_until" )
    var activeUntil: LocalDate? = null,

    @ApiModelProperty(hidden = true)
    @Column(length = 255) @Size(min = 0, max = 255)
    val testAnnotation: String? = null
) {

    companion object : KLogging()

    @Suppress("unused")
    private constructor() : this(name = "")

    override fun toString(): String {
        return "task[$id: $name, $activeFrom-$activeUntil]"
    }

    /** manual triggered validation. */
    fun validate(): Task {
        logger.info { "validate $this" }

        val validator = Validation.buildDefaultValidatorFactory().validator
        val validationResults = validator.validate(this)
        if (validationResults.isNotEmpty()) {
            throw ValidationException("not a valid task $this: ${validationResults.joinToString { "$it.message, " }}")
        }
        return this     // enable method chaining
    }
}
