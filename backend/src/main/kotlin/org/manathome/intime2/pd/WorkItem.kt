package org.manathome.intime2.pd

import com.fasterxml.jackson.annotation.JsonFormat
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import mu.KLogging
import org.springframework.format.annotation.DateTimeFormat
import java.math.BigDecimal
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.Validation
import javax.validation.ValidationException
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.NotNull
import javax.validation.constraints.PastOrPresent
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

const val WORK_ITEM_ID_UNDEFINED = -1L

/**
 *  work done on a give task (given day, in hours).
 *  @author man@home
 */
// tag::swagger[]
@ApiModel(value = "WorkItem", description =  "describing one unit of work (in hours) for a given day and task.") // <1>
@Entity
@Table(name = "it_workitem")
class WorkItem @JvmOverloads constructor(

    @ApiModelProperty("unique primary key", notes = "null only on new work items", example = "2")  // <2>
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = WORK_ITEM_ID_UNDEFINED,

// end::swagger[]

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @ApiModelProperty("date of day this work was done", notes = "date only, no time part", example = "2018-02-14")
    @get:NotNull
    val date: LocalDate = LocalDate.now(),

    @ApiModelProperty("number of hours worked this day on given task", example = "6")
    @DecimalMin("0")
    @get:NotNull
    @get:PositiveOrZero
    val hours: BigDecimal = BigDecimal.ZERO,

    @ManyToOne(cascade = [CascadeType.ALL], optional = false)
    @JoinColumn(name = "TASK_ID")
    @get:NotNull
    val task: Task? = null,

    @ApiModelProperty(hidden = true)
    @Column(length = 255)
    @get:Size(min = 0, max = 255)
    val testAnnotation: String? = null
) {

    companion object : KLogging()

    override fun toString(): String {
        return "Item[$id: ${date.format(DateTimeFormatter.ISO_DATE)}, hours: $hours, task: $task]"
    }

    /** manual triggered validation. */
    fun validate(): WorkItem {
        logger.info { "validate $this" }

        val validator = Validation.buildDefaultValidatorFactory().validator
        val validationResults = validator.validate(this)
        if (validationResults.isNotEmpty()) {
            throw ValidationException("not a valid work item $this: ${validationResults.joinToString { "$it.message, " }}")
        }
        return this     // enable method chaining
    }

}
