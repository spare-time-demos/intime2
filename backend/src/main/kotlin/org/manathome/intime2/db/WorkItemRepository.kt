package org.manathome.intime2.db

import org.manathome.intime2.pd.WorkItem
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.LocalDate

/**
 *   database repository for work items.
 *   @see WorkItem
 *   @author man@home
 */
interface WorkItemRepository : JpaRepository<WorkItem, Long> {

    /** find for given task. */
    fun findByTaskId(taskId: Long): Iterable<WorkItem>

    /**
     * finds all work in week.
     */
    @Query("SELECT wi FROM WorkItem wi WHERE  date >=:from AND date <= :until")
    fun findWorkInTimeRange(@Param("from") from: LocalDate, @Param("until") until: LocalDate): Iterable<WorkItem>
}
