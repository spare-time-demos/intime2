package org.manathome.intime2.db

import org.manathome.intime2.pd.Task
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.time.LocalDate
import javax.validation.constraints.NotEmpty

/**
 *  database repository for tasks.
 *   @see Task
 *   @author man@home
 */
@Component
@Validated
interface TaskRepository : JpaRepository<Task, Long> {

    /** find by task.name. */
    fun findByName(@NotEmpty name: String): Iterable<Task>

    /** find active tasks for given range (typically one week). */
    @Query("select t from Task t where (t.activeFrom IS NULL or t.activeFrom <= ?2) and (t.activeUntil IS NULL or t.activeUntil >= ?1)")
    fun findActiveTasks(
            from : LocalDate?,  // ?1
            until: LocalDate?   // ?2
    ): Iterable<Task>

}
