package org.manathome.intime2

import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 *   hello and root: dummy browser pages for rest service.
 *
 *   @author man@home
 */
@Controller
class HomeController {
    companion object : KLogging()

    @Value("\${info.build.version}")
    private var version: String? = null

    @Value("\${info.environment.display}")
    private var environmentDisplay: String? = null


    /** dummy root message for server. */
    @GetMapping("/info")
    @ResponseBody
    fun info(): String {
        logger.info { "/ call home. ${infoText()}" }

        return "<html><body><p>hello, this is the inTime2 backend</p><p>${infoText()}</p></body></html>"
    }

    /** short help text for intime2 /help. */
    @GetMapping("/help")
    fun help(): String {
        logger.info { "/help" }
        return "help" // thymeleaf template.
    }

    /** get version/profile information: info.build.version and info.environment.display.*/
    fun infoText(): String {
        return "$version - $environmentDisplay"
    }
}
