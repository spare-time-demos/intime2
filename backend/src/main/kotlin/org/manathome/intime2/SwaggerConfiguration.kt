package org.manathome.intime2

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 *  swagger json/rest documentation format configuration.
 *
 *  @author man@home
 *  @see <a href="http://localhost:8080/swagger-ui.html">test ui: http://localhost:8080/swagger-ui.html</a>
 *  @see <a href="http://localhost:8080/v2/api-docs">json file: http://localhost:8080/v2/api-docs</a>
 */
@Component
@EnableSwagger2
class SwaggerConfiguration {

    @Value("\${info.build.version}")
    private var version: String? = null

    @Suppress("unused")
    @Bean
    fun intime2Api(): Docket = Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.basePackage("org.manathome.intime2.api"))
            .paths(PathSelectors.any())
            .build()

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("inTime2 swagger REST API")
                .description("inTime2 swagger enabled rest api")
                .contact(Contact("man@home", "https://gitlab.com/man-at-home", null))
                .version("${version}")
                .build()
    }
}
