package org.manathome.intime2

import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

/**
 *   initializer code, runs once on startup. At the moment: only one version log entry.
 *
 *   @author a man@home
 */
@Suppress("unused")
@Component
class ApplicationStartupListener : ApplicationListener<ApplicationReadyEvent> {

    companion object : KLogging()

    @Value("\${info.build.version}")
    private var version: String? = null

    /**
     * This event is executed as late as conceivably possible to indicate that
     * the application is ready to service requests.
     */
    override fun onApplicationEvent(event: ApplicationReadyEvent) {

        logger.info { "intime2 running on version $version.." }
        return
    }

}



