package org.manathome.intime2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/** inTime2 application.
 *
 * @author man@home
 * */
@SpringBootApplication
class Intime2Application

    /** main starting point, running embedded tomcat server on port 8080. */
    fun main(args: Array<String>) {
        runApplication<Intime2Application>(*args)
    }

