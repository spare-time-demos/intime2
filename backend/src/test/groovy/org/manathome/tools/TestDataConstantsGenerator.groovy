package org.manathome.tools

import com.squareup.javapoet.FieldSpec
import com.squareup.javapoet.JavaFile
import com.squareup.javapoet.TypeName
import com.squareup.javapoet.TypeSpec
import groovy.io.FileType

import javax.lang.model.element.Modifier
import java.text.DateFormat

import static com.xlson.groovycsv.CsvParser.parseCsv

/**
/* Test Data Code Generator
/* Input:  CSV-File with Test Fixture
/* Output: Java Source Code with Constants
/* @author man@home
*/

println "  reading CSV.: ${this.args[0]}"
println "  writing Java: ${this.args[1]}"
println "         Class: ${this.args[2]}"
println "          ID..: ${this.args[3]}"


def javaSourceFile = new File("${this.args[1]}")
def csvDataText    = new File(this.args[0]).text
def csvFileName    = new File(this.args[0]).name
def csvFileAge     = new Date(new File(this.args[0]).lastModified()).format("yyyy-MM-dd'T'HH:mm'Z'")

// Read CSV test fixture data

def data           = parseCsv(csvDataText, separator: ';')

// Generate java source code:

def testClassBuilder = TypeSpec.classBuilder("${this.args[2]}")
        .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT)
        .addJavadoc("test data fixture constants, from csv file: ${csvFileName}.\n\n")
        .addJavadoc("<strong>GENERATED FILE, DO NOT EDIT</strong>\n\n")
        .addJavadoc("@author TestDataConstantsGenerator.groovy\n")
        .addJavadoc("@version ${csvFileAge}\n")

for(line in data) {

    def value = line[this.args[3]].toString().trim()
    FieldSpec dataConstant = FieldSpec.builder(long.class, "${this.args[3]}_${value}")
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC)
            .initializer("$value")
            .addJavadoc("csv: ${line}\n")
            .build();

    testClassBuilder.addField(dataConstant)
}

def testClass = testClassBuilder.build();

JavaFile javaFile = JavaFile.builder("org.manathome.intime2.generated", testClass)
        .build();

javaFile.writeTo javaSourceFile

println "  generation of test constants done."
