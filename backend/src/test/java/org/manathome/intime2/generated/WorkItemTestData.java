package org.manathome.intime2.generated;

/**
 * test data fixture constants, from csv file: it_workitem.testdata.csv.
 *
 * <strong>GENERATED FILE, DO NOT EDIT</strong>
 *
 * @author TestDataConstantsGenerator.groovy
 * @version 2018-03-12T21:42Z
 */
public abstract class WorkItemTestData {
  /**
   * csv: ID: 1 , DATE: 2018-03-13, HOURS:  1, TASK_ID:  1
   */
  public static final long ID_1 = 1;

  /**
   * csv: ID: 2 , DATE: 2018-03-14, HOURS:  2, TASK_ID:  1
   */
  public static final long ID_2 = 2;

  /**
   * csv: ID: 3 , DATE: 2018-03-16, HOURS:  3, TASK_ID:  1
   */
  public static final long ID_3 = 3;

  /**
   * csv: ID: 4 , DATE: 2018-03-17, HOURS:  5, TASK_ID:  1
   */
  public static final long ID_4 = 4;

  /**
   * csv: ID: 5 , DATE: 2018-03-14, HOURS:  6, TASK_ID:  2
   */
  public static final long ID_5 = 5;

  /**
   * csv: ID: 6 , DATE: 2018-03-17, HOURS:  6, TASK_ID:  2
   */
  public static final long ID_6 = 6;

  /**
   * csv: ID: 7 , DATE: 2018-03-15, HOURS:  2, TASK_ID:  4
   */
  public static final long ID_7 = 7;

  /**
   * csv: ID: 8 , DATE: 2018-03-16, HOURS:  3, TASK_ID:  4
   */
  public static final long ID_8 = 8;

  /**
   * csv: ID: 9 , DATE: 2018-03-20, HOURS:  8, TASK_ID:  1
   */
  public static final long ID_9 = 9;

  /**
   * csv: ID: 10, DATE: 2018-03-21, HOURS:  2, TASK_ID:  1
   */
  public static final long ID_10 = 10;

  /**
   * csv: ID: 11, DATE: 2018-03-21, HOURS:  2, TASK_ID:  4
   */
  public static final long ID_11 = 11;

  /**
   * csv: ID: 12, DATE: 2018-03-30, HOURS:  1, TASK_ID:  1
   */
  public static final long ID_12 = 12;
}
