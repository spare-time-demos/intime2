package org.manathome.intime2.generated;

/**
 * test data fixture constants, from csv file: it_task.testdata.csv.
 *
 * <strong>GENERATED FILE, DO NOT EDIT</strong>
 *
 * @author TestDataConstantsGenerator.groovy
 * @version 2018-03-27T22:56Z
 */
public abstract class TaskTestData {
  /**
   * csv: ID: 1, NAME:  testtask 1
   */
  public static final long ID_1 = 1;

  /**
   * csv: ID: 2, NAME:  a testtask 2
   */
  public static final long ID_2 = 2;

  /**
   * csv: ID: 3, NAME:  third empty task
   */
  public static final long ID_3 = 3;

  /**
   * csv: ID: 4, NAME:  testtask 4
   */
  public static final long ID_4 = 4;
}
