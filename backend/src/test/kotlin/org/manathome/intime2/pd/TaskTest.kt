package org.manathome.intime2.pd

import com.tngtech.jgiven.Stage
import com.tngtech.jgiven.junit5.SimpleScenarioTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.context.junit.jupiter.SpringExtension
import javax.validation.ValidationException

/**
 *   test Tasks class.
 *   @author man@home
 */
@ExtendWith(SpringExtension::class)
@Tag("feature:task-behavior")
//@Epic("Tasks Management")
//@Feature("Task POJO")
@DisplayName("task tests - task behavior")
open class TaskTest : SimpleScenarioTest<TaskTest.TaskTestSteps>() {

    @Test
    open fun scenario_task_creation() {

        given()
                .created_task("SAMPLE-TASK")
        then()
                .has_Name("SAMPLE-TASK")
                .and()
                .hasEqualsAnHashcode()
                .and()
                .validation_should_succeed()
    }

    @Test
    open fun scenario_task_copy() {

        given()
                .created_task("sample_copy_task")
        `when`()
                .copy_task("sample_copy_task")
        then()
                .has_Name("sample_copy_task")
                .and()
                .copyHasSameEqualsAnHashcode()
    }

    @Test
    open fun scenario_empty_task_validation() {

        given()
                .created_task("")
        then()
                .validation_should_fail()
    }

    open class TaskTestSteps : Stage<TaskTestSteps>() {

        private var task: Task? = null

        private var taskCopy: Task? = null

        open fun created_task(name: String): TaskTestSteps {
            this.task = Task(name = name)
            return this
        }

        open fun has_Name(name: String): TaskTestSteps {
            Assertions.assertEquals(name, this.task!!.name)
            return this
        }

        open fun hasEqualsAnHashcode() : TaskTestSteps {
            Assertions.assertTrue(task!! == task)
            Assertions.assertEquals(task!!.hashCode(), task!!.hashCode())
            return this
        }

        open fun copyHasSameEqualsAnHashcode() : TaskTestSteps {
            Assertions.assertTrue(task!! == taskCopy)
            Assertions.assertEquals(task!!.hashCode(), taskCopy!!.hashCode())
            return this
        }

        open fun copy_task(newName: String) : TaskTestSteps {
            this.taskCopy = task!!.copy( name =  newName)
            return this
        }

        open fun validation_should_fail() : TaskTestSteps {
            try {
                this.task!!.validate()
                Assertions.fail<Nothing>("expected a validation error on task $this.task")
            }catch(ve: ValidationException) {
                // ok.
            }
            return this
        }

        open fun validation_should_succeed() : TaskTestSteps {
            this.task!!.validate()
            return this
        }

    }

}
