package org.manathome.intime2.pd

import com.tngtech.jgiven.Stage
import com.tngtech.jgiven.junit5.SimpleScenarioTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.LocalDate
import javax.validation.ValidationException

/**
 *   bdd style test for work items.
 *   @author man@home
 */
@Tag("feature:work-item-behavior")
@DisplayName("work-item-tests - workt-item behavior")
open class WorkItemTest : SimpleScenarioTest<WorkItemTest.WorkItemTestSteps>() {

    @Test
    open fun scenario_create_WorkItem() {

        given()
                .work_hours(10)
        `when`()
                .the_work_item_is_created()
        then()
                .the_workitem_has_hours_set_to(10)
                .and()
                .work_item_validation_should_succeed()

    }

    @Test
    open fun scenario_create_invalid_WorkItem() {
        given()
                .work_hours(-1)
        `when`()
                .the_work_item_is_created()
        then()
                .work_item_validation_should_fail()

    }

    open class WorkItemTestSteps : Stage<WorkItemTestSteps>() {

        private val task: Task = Task(-1,"work-item-test-task")
        private var hours: Int? = null
        private var workItem: WorkItem? = null

        open fun work_hours(hours: Int): WorkItemTestSteps {
            this.hours = hours
            return this
        }

        open fun the_work_item_is_created(): WorkItemTestSteps {
            workItem = WorkItem(-1, LocalDate.now(), BigDecimal(hours!!), task)
            return this
        }

        open fun the_workitem_has_hours_set_to(hours: Int): WorkItemTestSteps {
            Assertions.assertEquals(BigDecimal(hours), workItem!!.hours)
            return this
        }

        open fun work_item_validation_should_fail(): WorkItemTestSteps {
            try {
                this.workItem!!.validate()
                Assertions.fail<Nothing>("expected a validation error on workitem ${this.workItem}")
            } catch (ve: ValidationException) {
                // ok.
            }
            return this
        }

        open fun work_item_validation_should_succeed(): WorkItemTestSteps {
            this.workItem!!.validate()
            return this
        }

    }


}



