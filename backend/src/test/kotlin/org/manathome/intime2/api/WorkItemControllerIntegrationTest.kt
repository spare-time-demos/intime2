package org.manathome.intime2.api

import com.tngtech.jgiven.Stage
import com.tngtech.jgiven.annotation.As
import com.tngtech.jgiven.annotation.ExtendedDescription
import com.tngtech.jgiven.annotation.Hidden
import com.tngtech.jgiven.junit5.SimpleScenarioTest
import mu.KLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.manathome.intime2.pd.WorkItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal
import java.time.LocalDate

/**
 *  testing actual http endpoints of rest controller.
 *  @author man@home
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@Tag("rest-api")
@DisplayName("WorkItemController: Rest API WorkItems")
class WorkItemControllerIntegrationTest : SimpleScenarioTest<RestCallSteps>() {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var workItemRepository: WorkItemRepository

    @Autowired
    lateinit var taskRepository: TaskRepository

    @Test
    @Tag("feature:work-item-api")
    @DisplayName("work item tests - retrieve work items for week")
    fun retrieve_Work_For_Week() {

        val lastWeek = LocalDate.now().minusWeeks(1)
        workItemRepository.save(WorkItem(date = lastWeek, hours = BigDecimal(3.0), task = Task(name = "w1-test-task1 day1")))
        workItemRepository.save(WorkItem(date = lastWeek.plusDays(1), hours = BigDecimal(4.0), task = Task(name = "w2-test-task2 day2")))

        given()
                .init(mvc)

        `when`()
                .get("/work/week/$lastWeek")

        then()
                .response_http_ok()
                .and()
                .expect_Response_with_Json("$[0].hours","3.0")
                .and()
                .expect_Response_with_Json("$[1].hours","4.0")
    }

    @Test
    @Tag("feature:work-item-api")
    @DisplayName("work item tests - work for tasks")
    fun retrieve_Work_For_Task() {

        given()
                .init(mvc)

        `when`()
                .get("/task/1/work")

        then()
                .response_http_ok()
                .and()
                .response_contains_content("2018-03-16")
    }


    @Test
    @Tag("feature:work-item-api")
    @DisplayName("work item tests - save work items")
    fun save_WorkItems() {

        val date = LocalDate.of(2017, 10, 2)

        val t1 = taskRepository.saveAndFlush(Task(-1, "saved-dummy-task1"))
        val t2 = taskRepository.saveAndFlush(Task(-1, "saved-dummy-task2"))

        Assertions.assertTrue(t2.id > 4, "task insert should create id AFTER test fixture data, not ${t2.id}")

        val wi = workItemRepository.saveAndFlush(WorkItem(date = date, hours = BigDecimal(3.0), task = t1))

        val json = """
            [
            {"id":${wi.id},"date":[2017,10,2],"hours":8.00,"task":{"id":${t1.id},"name":"${t1.name}"}},
            {"id":-1,      "date":[2017,10,3],"hours":7,   "task":{"id":${t2.id},"name":"${t2.name}"}},
            {"id":-1,      "date":[2017,10,3],"hours":1,   "task":{"id":${t2.id},"name":"${t2.name}"}},
            {"id":-1,      "date":"2017-10-04","hours":2, "task":{"id":${t2.id},"name":"${t2.name}","activeFrom":"2000-01-01","activeUntil":"2100-12-30"}}
            ]
            """

        given()
                .init(mvc)

        `when`()
                .post("/work", json)

        then()
                .response_http_ok()
                .and()
                .response_contains_content("3xxxxx")

        val wis = workItemRepository.findWorkInTimeRange(date.minusDays(1), date.plusDays(6))
        Assertions.assertTrue(wis.count() >= 1)
        wis.forEach { logger.info { it } }
        Assertions.assertTrue(wis.any { it.date == date },"should find saved $date item.")
        Assertions.assertEquals(wi.id, wis.firstOrNull { it.date == date }?.id,"one item with id ${wi.id} found")
        Assertions.assertEquals(8, wis.firstOrNull { it.date == date && it.id == wi.id }?.hours?.toInt(),"changed hours from 3 to 8")
        Assertions.assertEquals(t1.name, wis.firstOrNull { it.id == wi.id }?.task?.name)

        Assertions.assertEquals(2, wis.count { it.date == date.plusDays(1) }, "should find 2 saved ${date.plusDays(1)} items.")
        Assertions.assertFalse(wis.any { it.date == date.plusDays(1) && it.task?.id != t2.id },"no new workItem for existing task t1")
    }
}

open class RestCallSteps : Stage<RestCallSteps>() {

    private var mvc: MockMvc? = null
    private var result: ResultActions? = null


    @Hidden
    open fun getResultText(): String {
        return result?.andReturn()?.response?.contentAsString ?: "no result";
    }

    @Hidden
    open fun init(mvc: MockMvc): RestCallSteps {
        this.mvc = mvc
        return this
    }

    @As("a http GET request of $")
    @ExtendedDescription("request spring boots MockMvc.perform(get())")
    open fun get(url: String): RestCallSteps {

        WorkItemControllerIntegrationTest.logger.info { "calling http get $url" }

        result = this.mvc!!
                .perform(MockMvcRequestBuilders.get(url))
                .andDo { MockMvcResultHandlers.print() }

        WorkItemControllerIntegrationTest.logger.info { getResultText() }

        return this
    }

    @As("a http POST request of $")
    @ExtendedDescription("request spring boots MockMvc.perform(post())")
    open fun post(url: String, json: String): RestCallSteps {

        WorkItemControllerIntegrationTest.logger.info { "calling http post $url" }

        result = this.mvc!!
                .perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())

        return this
    }

    open fun expect_Response_with_Json(matcher: ResultMatcher): RestCallSteps {

        this.result!!.andExpect(matcher)
        return this
    }

    open fun expect_Response_with_Json(match: String, value: String): RestCallSteps {
        return expect_Response_with_Json(jsonPath(match).value(value))
    }

    @As("response contains content: $")
    open fun response_contains_content(content: String): RestCallSteps {
        result!!.andReturn()
                .response
                .contentAsString
                .contains(content)

        return this
    }

    @As("response http return code is HTTP_OK")
    open fun response_http_ok(): RestCallSteps {
        result!!.andExpect(status().isOk)
        return this
    }
}
