package org.manathome.intime2.api

import mu.KLogging
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * testing stubbed out trace controller.
 * @author man@home
 */
@ExtendWith(SpringExtension::class)
@WebMvcTest(TraceController::class)
@Tag("rest-api")
class TraceControllerUnitTest {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Test
    fun log() {

        val json = """
            [
            {"msg":"rabarba rabarba"},
            {"msg":"log message 2"}
            ]
            """
        mvc
                .perform(post("/trace/log")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk)
    }

    @Test
    fun trackWithData() {

        val json = """
            [
            {"page":"search"},
            {"page":"search", "action": "search", "data": "f"},
            {"page":"search", "action": "page"},
            {"page":"search", "action": "select", "data": "fondak"},
            {"page":"search", "action": "logout"}
            ]
            """
        mvc
                .perform(post("/trace/track")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk)
    }

    @Test
    fun trackWithoutData() {

        val json = """
            []
            """

        mvc
                .perform(post("/trace/track")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(print())
                .andExpect(status().isOk)
    }

    @Test
    fun trackWithNothing() {

        mvc
                .perform(post("/trace/track")
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(print())
                .andExpect(status().isOk)
    }

}

