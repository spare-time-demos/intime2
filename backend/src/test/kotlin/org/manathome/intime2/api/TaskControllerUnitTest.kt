package org.manathome.intime2.api

import mu.KLogging
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.mockito.BDDMockito
import org.mockito.BDDMockito.willReturn
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDate

/**
 *   @author a man@home
 */
@ExtendWith(SpringExtension::class)
@WebMvcTest(TaskController::class)
open class TaskControllerUnitTest {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @MockBean
    private lateinit var taskRepository: TaskRepository

    @MockBean
    private lateinit var workItemRepository: WorkItemRepository

    @Test
    @DisplayName("find tasks rest call 2")
    open fun findTasks() {

        val until = LocalDate.now().plusDays(1)

        BDDMockito.given(this.taskRepository.findAll())
                .willReturn(
                        arrayListOf(
                                Task(5, "5-task"),
                                Task(6, "6er Task with from", LocalDate.now().minusDays(1)),
                                Task(7, "7er Task with until", null, until)
                        )
                )

        mvc
                .perform(MockMvcRequestBuilders.get("/task"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("\$[0].id").value("5"))
                .andExpect(MockMvcResultMatchers.jsonPath("\$[1].name").value("6er Task with from"))
                .andExpect(MockMvcResultMatchers.jsonPath("\$[2].activeUntil").value(until!!.toString()))
                .andReturn()
    }


    @Test
    @DisplayName("save new task")
    fun saveNewTask() {

        val t = Task(-1,"new task to save")

        BDDMockito.given(this.taskRepository.save(t))
                .willReturn(Task(37,"savedTask"))


        val json =
                """
                {"id": ${t.id}, "name": "${t.name}"}
                """
        mvc
                .perform(MockMvcRequestBuilders.post("/saveTask")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("id").value("37"))
    }

    @Test
    @DisplayName("delete task")
    fun deleteTask() {

        val id: Long = 444;
        BDDMockito.given(this.taskRepository.deleteById(id)).willAnswer { Unit }

        val json =
                """
                ${id}
                """
        mvc
                .perform(MockMvcRequestBuilders.post("/deleteTask")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().string("${id}"));
    }

}
