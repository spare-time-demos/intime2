package org.manathome.intime2.api

import com.tngtech.jgiven.Stage
import com.tngtech.jgiven.annotation.As
import com.tngtech.jgiven.annotation.ExtendedDescription
import com.tngtech.jgiven.annotation.Hidden
import com.tngtech.jgiven.junit5.SimpleScenarioTest
import mu.KLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.manathome.intime2.pd.WORK_ITEM_ID_UNDEFINED
import org.manathome.intime2.pd.WorkItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal
import java.time.LocalDate

/**
 *  testing actual http endpoints of rest controller.
 *  @author man@home
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@Tag("rest-api")
@DisplayName("WorkItemController: Rest API WorkItems")
class TaskControllerIntegrationTest : SimpleScenarioTest<TaskRestCallSteps>() {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var workItemRepository: WorkItemRepository

    @Autowired
    lateinit var taskRepository: TaskRepository


    @Test
    @Tag("feature:task-api")
    @DisplayName("task tests - task api retrieve")
    fun retrieve_Tasks_For_Week() {

        val today = LocalDate.now()
        taskRepository.save(Task(name = "newly-saved-test-task"))

        given()
                .init(mvc)
        `when`()
                .get("/task/week/$today")

        then()
                .response_http_ok()
                .and()
                .response_contains_content("newly-saved-test-task")
    }

    @Test
    @Tag("feature:task-api")
    @DisplayName("task item tests - delete task with work failes")
    fun delete_Task_with_Work_Failes() {

        val json = """
            4
            """

        given()
                .init(mvc, taskRepository, workItemRepository)
                .a_task_with_work()

        `when`()
                .post("/deleteTask", json)

        then()
                .response_http_nok(400)
    }
}

open class TaskRestCallSteps : Stage<TaskRestCallSteps>() {

    private var mvc: MockMvc? = null
    private var result: ResultActions? = null

    private var workItemRepository: WorkItemRepository? = null
    private var taskRepository: TaskRepository? = null

    var task: Task? = null

    @Hidden
    open fun init(mvc: MockMvc,
                  taskRepository: TaskRepository? = null,
                  workItemRepository: WorkItemRepository? = null): TaskRestCallSteps {

        this.mvc = mvc
        this.taskRepository = taskRepository
        this.workItemRepository = workItemRepository

        return this
    }

    @As("a http GET request of $")
    @ExtendedDescription("request spring boots MockMvc.perform(get())")
    open fun get(url: String): TaskRestCallSteps {

        TaskControllerIntegrationTest.logger.info { "calling http get $url" }

        result = this.mvc!!
                .perform(MockMvcRequestBuilders.get(url))
                .andDo { MockMvcResultHandlers.print() }

        return this
    }

    @As("a http POST request of $")
    @ExtendedDescription("request spring boots MockMvc.perform(post())")
    open fun post(url: String, json: String): TaskRestCallSteps {

        TaskControllerIntegrationTest.logger.info { "calling http post $url" }

        result = this.mvc!!
                .perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())

        return this
    }

    open fun expect_Response_with_Json(matcher: ResultMatcher): TaskRestCallSteps {

        this.result!!.andExpect(matcher)
        return this
    }

    open fun expect_Response_with_Json(match: String, value: String): TaskRestCallSteps {
        return expect_Response_with_Json(jsonPath(match).value(value))
    }

    @As("response contains content: $.")
    open fun response_contains_content(content: String): TaskRestCallSteps {
        result!!.andReturn()
                .response
                .contentAsString
                .contains(content)

        return this
    }

    @As("response http return code is HTTP_OK.")
    open fun response_http_ok(): TaskRestCallSteps {
        result!!.andExpect(status().isOk)
        return this
    }

    @As("response http return code is $.")
    open fun response_http_nok(code: Int): TaskRestCallSteps {
        result!!.andExpect(status().`is`(code))
        return this
    }

    fun a_task_with_work() {

        val dt = LocalDate.of(2016,1,2)
        val t = Task(-1, "new task with work unit test created.", dt, dt.plusDays(2))
        this.task = taskRepository!!.saveAndFlush(t)

        val wi = WorkItem(WORK_ITEM_ID_UNDEFINED, dt.plusDays(1), BigDecimal("3"), this.task)

        workItemRepository!!.saveAndFlush(wi)
    }
}
