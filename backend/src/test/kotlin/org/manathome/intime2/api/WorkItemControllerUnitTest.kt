package org.manathome.intime2.api

import mu.KLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.manathome.intime2.pd.WorkItem
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal
import java.time.LocalDate

/**
 * testing stubbed out rest controller.
 * @author man@home
 */
@ExtendWith(SpringExtension::class)
@WebMvcTest(WorkItemController::class)
class WorkItemControllerUnitTest {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @MockBean
    private lateinit var workItemRepository: WorkItemRepository

    @MockBean
    private lateinit var taskItemRepository: TaskRepository

    @Test
    fun findWorkForWeek() {

        val from: LocalDate = LocalDate.of(2017, 10, 11)
        val until = from.plusDays(7)

        given(this.workItemRepository.findWorkInTimeRange(from, until))
                .willReturn(
                        arrayListOf(WorkItem(42L, from.plusDays(1), BigDecimal("3"), Task(5, "fix task")))
                )

        given(this.taskItemRepository.count())
                .willReturn(0) // not necessary

        val result = mvc
                .perform(get("/work/week/2017-10-11"))
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("\$[0].id").value("42"))
                .andExpect(jsonPath("\$[0].hours").value("3"))
                .andExpect(jsonPath("\$[0].task.id").value("5"))
                .andExpect(jsonPath("\$[0].date").value("2017-10-12"))
                .andReturn()

        Assertions.assertNotNull(result.response.contentAsString)
    }
}
