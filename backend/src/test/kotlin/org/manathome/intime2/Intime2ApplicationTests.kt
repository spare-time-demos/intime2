package org.manathome.intime2

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@SpringBootTest
@DisplayName("intime2 application build infrastructure")
class Intime2ApplicationTests {

    @Test
    @DisplayName("load spring test context")
    fun contextLoads() {
    }
}
