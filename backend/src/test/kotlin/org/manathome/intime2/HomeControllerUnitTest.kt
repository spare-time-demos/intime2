package org.manathome.intime2

import com.tngtech.jgiven.Stage
import com.tngtech.jgiven.annotation.As
import com.tngtech.jgiven.annotation.ExtendedDescription
import com.tngtech.jgiven.junit5.SimpleScenarioTest
import mu.KLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders

/**
 * testing /info and /help endpoints.
 *
 * @author man@home
 */
@ExtendWith(SpringExtension::class)
@WebMvcTest(HomeController::class)
@DisplayName("Home Controller: Server side rendered web page tests")
@Tag("web")
open class HomeControllerUnitTest : SimpleScenarioTest<RestSteps>() {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var controller: HomeController

    @Test
    open fun render_html_info_static_html() {

        given()
                .init(mvc)
        `when`()
                .get("/info")
        then()
                .response_http_ok()
                .and()
                .response_contains_content("hello, this is the inTime2 backend")
    }

    @Test
    open fun render_html_help_thymeleaf_template() {

        given()
                .init(mvc)
        `when`()
                .get("/help")
        then()
                .response_http_ok()
                .and()
                .response_contains_content("getting help is overrated..")
    }

    @Test
    open fun versionInfo() {

        given()
                .controller(this.controller)
        then()
                .got_version_info()
    }

}

/** jgiven test steps. */
open class RestSteps : Stage<RestSteps>() {

    companion object : KLogging()

    private var mvc: MockMvc? = null
    private var result: ResultActions? = null
    private var controller: HomeController? = null

    @As("a started spring boot backend")
    open fun init(mvc: MockMvc): RestSteps {
        this.mvc = mvc
        return this
    }

    @As("the spring boot home controller")
    open fun controller(controller: HomeController): RestSteps {
        this.controller = controller
        return this
    }

    @As("requesting a http GET request of $")
    @ExtendedDescription("request spring boots MockMvc.perform(get())")
    open fun get(url: String): RestSteps {
        result = this.mvc!!
                .perform(MockMvcRequestBuilders.get(url))
                .andDo { print() }

        return this
    }

    @As("the response contains content: $")
    open fun response_contains_content(content: String): RestSteps {
        result!!.andReturn()
                .response
                .contentAsString
                .contains(content)

        return this
    }

    @As("the response http return code is HTTP_OK")
    open fun response_http_ok(): RestSteps {
        result!!.andExpect(status().isOk)
        return this
    }

    open fun got_version_info(): RestSteps {

        Assertions.assertNotNull(this.controller)
        Assertions.assertNotNull(this.controller?.infoText())

        val msg = "environment during this test is: ${this.controller?.infoText()} "
        println(msg)
        logger.warn { msg }
        return this
    }
}
