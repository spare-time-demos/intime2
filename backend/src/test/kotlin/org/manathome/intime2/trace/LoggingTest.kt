package org.manathome.intime2.trace

import mu.KLogging
import org.junit.jupiter.api.Test

/**
 *  test logging messages. may not break the code.
 *
 *  @author a man@home
 */
class LoggingTest {

    companion object : KLogging()

    @Test
    fun sendLogMessagesWithDifferentLevels() {
        logger.trace { "unit test trace message" };
        logger.debug { "unit test debug message" };
        logger.info { "unit test info message" };
        logger.warn { "unit test warn message, should be forwarded to sentry.io" };
        logger.error { "unit test error message, should be forwarded to sentry.io" };

    }

    @Test
    fun sendEmptyLogMessage() {
        logger.debug {};
    }
}
