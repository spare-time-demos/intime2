package org.manathome.intime2.trace

import mu.KLogging
import org.junit.Assume
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.net.SocketTimeoutException

/**
 * test sending data to google analytics (often won't work in gitlab-ci)
 *
 *   @author a man@home
 *   @since  2018-05-05
 */
@Tag("external-api")
class GoogleAnalyticsForwaderTest {
    companion object : KLogging()

    @Disabled("will not work on gitlab")
    @Test
    fun sendTrackingData() {

        val gaf = GoogleAnalyticsForwarder()

        val userAgent = "junit-5"
        val clientId  = "junit-1"
        val clientIp  = "193.17.27.15"

        val trackData = arrayOf(
                TrackData(page = "unit-test-page1a"),
                TrackData(page = "unit-test-page2a"),
                TrackData(page = "unit-test-page2a", action = "page", data = "next"),
                TrackData(page = "unit-test-page2a", action = "page", data = "next"),
                TrackData(page = "unit-test-page2a", action = "page", data = "previous"),
                TrackData(page = "unit-test-page2a", action = "logout")
        )

        try {
            val count = gaf.send(trackData, userAgent, clientId, clientIp)
            Assertions.assertEquals(count, 6)

        } catch(ex: SocketTimeoutException) {
            // throw AssumptionViolatedException("inconclusive, Socket Timeout for Google Analytics. $ex")
            Assume.assumeTrue(false)
            // inconclusive not working.. fall through
        }
    }

    @Disabled("will not work on gitlab")
    @Test
    fun validateTrackingDataOk() {

        val gaf = GoogleAnalyticsForwarder(GoogleAnalyticsForwarder.googleAnalyticsDebugUrl)

        val userAgent = "junit-userAgent"
        val clientId  = "junit-validated-client"
        val clientIp  = "193.1.1.15"

        val trackData = arrayOf(
                TrackData(page = "unit-test-page1b"),
                TrackData(page = "unit-test-page2b", action = "page", data = "next"),
                TrackData(page = "unit-test-page2b", action = "logout")
        )

        try {
            val count = gaf.send(trackData, userAgent, clientId, clientIp)
            Assertions.assertEquals(count, 3)
        } catch(ex: SocketTimeoutException) {
            Assume.assumeTrue(false)
            // throw AssumptionViolatedException("inconclusive, Socket Timeout for Google Analytics. $ex")
        }
    }

    @Disabled("will not work on gitlab")
    @Test
    fun validateInvalidTrackingDataWithoutPageData() {

        val gaf = GoogleAnalyticsForwarder(GoogleAnalyticsForwarder.googleAnalyticsDebugUrl)

        val userAgent = "junit-val-c"
        val clientId  = "junit-val-c1"
        val clientIp  = "193.17.27.15"

        val trackData = arrayOf(
                TrackData(page = "")
        )

        try {
            gaf.send(trackData, userAgent, clientId, clientIp)
            Assertions.fail<Nothing>("expected exception, missing page data for google..")
        }catch(iae: IllegalArgumentException) {
            // ok
        }catch(ex: SocketTimeoutException) {
            // throw AssumptionViolatedException("inconclusive, Socket Timeout for Google Analytics. $ex")
        }
    }


    @Disabled("will not work on gitlab")
    @Test
    fun validateInvalidTrackingDataWithoutClientId() {

        val gaf = GoogleAnalyticsForwarder(GoogleAnalyticsForwarder.googleAnalyticsDebugUrl)

        val userAgent = "junit-val-d"
        val clientId  = ""
        val clientIp  = "193.17.27.15"

        val trackData = arrayOf(
                TrackData(page = "invalidIpOnThisPage")
        )

        Assertions.assertThrows(IllegalArgumentException::class.java)
        {
            gaf.send(trackData, userAgent, clientId, clientIp)
        }
    }
}
