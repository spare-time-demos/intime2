package org.manathome.intime2.docs

import mu.KLogging
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.io.File
import java.nio.file.Paths

/**
 * extract swagger json schema from running spring app http://localhost:8080/v2/api-docs.
 * @author man@home
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@Tag("doc")
@DisplayName("swagger json schema generation from /v2/api-docs.")
class SwaggerSchemaGenerationTest {

    val jsonTargetPath = "build/api-docs"

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Test
    fun extractJsonFromUrl() {

        val swaggerJsonSchemaResult = mvc
                .perform(MockMvcRequestBuilders
                        .get("/v2/api-docs")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andReturn()

        val swaggerJsonSchema =swaggerJsonSchemaResult.response.contentAsString
        Assertions.assertThat(swaggerJsonSchema).contains("\"swagger\"")
        Assertions.assertThat(swaggerJsonSchema).contains("inTime2")

        val path = Paths.get("").toAbsolutePath().toString()
        logger.info { "Working Directory = $path" }
        File(jsonTargetPath).mkdirs()
        File("$jsonTargetPath/intime2.swagger.json").writeText(swaggerJsonSchema)
    }

}
