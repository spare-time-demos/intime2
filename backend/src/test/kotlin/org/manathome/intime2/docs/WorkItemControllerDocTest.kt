package org.manathome.intime2.docs

import mu.KLogging
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.api.WorkItemControllerIntegrationTest
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.db.WorkItemRepository
import org.manathome.intime2.pd.Task
import org.manathome.intime2.pd.WorkItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.operation.preprocess.Preprocessors
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigDecimal
import java.time.LocalDate

/**
 * using RESTDoc to generate parts of the REST API Documentation.
 * @author man@home
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs("build/api-docs/snippets", uriPort = 80, uriHost = "api.intime2.manathome.org", uriScheme = "http")
@Tag("doc")
@DisplayName("generate RestDoc documentation for WorkItemController.")
class WorkItemControllerDocTest {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var workItemRepository: WorkItemRepository

    @Autowired
    private lateinit var taskRepository: TaskRepository

    @Test
    @DisplayName("generate RESTDoc documentation for /work/week/.")
    fun work_week() {

        val today = LocalDate.now()
        workItemRepository.saveAndFlush(WorkItem(date = today, hours = BigDecimal(3.0), task = Task(name = "test-task1")))
        workItemRepository.saveAndFlush(WorkItem(date = today, hours = BigDecimal(4.0), task = Task(name = "test-task2")))

        mvc
                .perform(MockMvcRequestBuilders
                        .get("/work/week/$today")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray)
                .andDo(document("work-week", Preprocessors.preprocessResponse(Preprocessors.prettyPrint())))
    }

    @Test
    @DisplayName("generate RESTDoc documentation for /task/week/.")
    fun task_week() {
        val today = LocalDate.now()
        taskRepository.saveAndFlush(Task(name = "first-task"))
        taskRepository.saveAndFlush(Task(name = "second-task"))

        val url = "/task/week/$today"
        logger.info { "calling $url" }

        mvc
                .perform(MockMvcRequestBuilders.get(url))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(document("tasks-week", Preprocessors.preprocessResponse(Preprocessors.prettyPrint())))
    }
}
