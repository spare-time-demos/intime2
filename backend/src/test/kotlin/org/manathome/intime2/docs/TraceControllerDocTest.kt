package org.manathome.intime2.docs

import mu.KLogging
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.operation.preprocess.Preprocessors
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

/**
 * using RESTDoc to generate parts of the REST API Documentation.
 * @author man@home
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs("build/api-docs/snippets", uriPort = 80, uriHost = "api.intime2.manathome.org", uriScheme = "http")
@Tag("doc")
@DisplayName("generate RestDoc documentation for TraceController.")
class TraceControllerDocTest {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc


    @Test
    @DisplayName("generate RESTDoc documentation for /trace/track.")
    fun trace_track() {

        val json = """
            [
                {"page":"/search"},
                {"page":"/found"}
            ]
            """

        mvc
                .perform(MockMvcRequestBuilders
                        .post("/trace/track")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andDo(document("trace-track", Preprocessors.preprocessResponse(Preprocessors.prettyPrint())))
    }

}
