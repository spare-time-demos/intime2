package org.manathome.intime2.docs

import mu.KLogging
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.db.TaskRepository
import org.manathome.intime2.pd.Task
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation
import org.springframework.restdocs.operation.preprocess.Preprocessors
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.time.LocalDate

/**
 *   @author a man@home
 */
@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs("build/api-docs/snippets", uriPort = 80, uriHost = "api.intime2.manathome.org", uriScheme = "http")
@Tag("doc")
@DisplayName("generate RestDoc documentation for TaskController.")
class TaskControllerDocTest {

    companion object : KLogging()

    @Autowired
    private lateinit var mvc: MockMvc

    @Autowired
    private lateinit var taskRepository: TaskRepository

    @Test
    @DisplayName("generate RESTDoc documentation for /task.")
    fun task() {

        mvc
                .perform(MockMvcRequestBuilders
                        .get("/task")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray)
                .andDo(MockMvcRestDocumentation.document(
                        "task",
                        Preprocessors.preprocessResponse(Preprocessors.prettyPrint())))
    }

    @Test
    @DisplayName("generate RESTDoc documentation for /saveTask.")
    fun saveTask() {

        val t = Task(name = "a new task to save", activeFrom = LocalDate.now())

        val json =
                """
                {"id": ${t.id}, "name": "${t.name}"}
                """

        mvc
                .perform(MockMvcRequestBuilders
                        .post("/saveTask")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.jsonPath("id").isNumber)
                .andDo(MockMvcRestDocumentation.document(
                        "saveTask",
                        Preprocessors.preprocessResponse(Preprocessors.prettyPrint())))
    }

    @Test
    @DisplayName("generate RESTDoc documentation for /deleteTask.")
    fun deleteTask() {

        val t = Task(name = "a new task to savely delete", activeFrom = LocalDate.now())
        val savedTask = taskRepository.saveAndFlush(t);

        val json =
                """
                ${savedTask.id}
                """

        mvc
                .perform(MockMvcRequestBuilders
                        .post("/deleteTask")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(json)
                        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(MockMvcResultMatchers.content().string("${savedTask.id}"))
                .andDo(MockMvcRestDocumentation.document(
                        "deleteTask",
                        Preprocessors.preprocessResponse(Preprocessors.prettyPrint())))
    }

}
