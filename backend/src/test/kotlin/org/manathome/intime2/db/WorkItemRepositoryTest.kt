package org.manathome.intime2.db

import mu.KLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.generated.WorkItemTestData
import org.manathome.intime2.pd.Task
import org.manathome.intime2.pd.WorkItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.math.BigDecimal
import java.time.LocalDate

/**
 *  testing database access work items.
 *   @author man@home
 */
@ExtendWith(SpringExtension::class)
@DataJpaTest
@Tag("db")
class WorkItemRepositoryTest {

    companion object : KLogging()

    @Autowired
    lateinit var repository: WorkItemRepository

    @Test
    fun persistenceTest() {
        val t = Task(id = 0, name = "test-task1")
        val wi = WorkItem(task = t, date = LocalDate.now(), hours = BigDecimal(3L))
        val savedT = repository.save(wi)

        Assertions.assertNotNull(savedT)
        Assertions.assertTrue(savedT.id > 0)

        val loadedT = repository.findById(savedT.id).get()
        Assertions.assertEquals(savedT.id, loadedT.id)
        Assertions.assertEquals(savedT.task?.id, loadedT.task?.id)

        repository.deleteById(loadedT.id)
    }

    @Test
    fun queryTest() {

        val d = LocalDate.now().minusMonths(5)
        val t = Task(1, "unit-test-task-2")
        repository.save(WorkItem(task = t, date = d, hours = BigDecimal(3L)))
        repository.save(WorkItem(task = t, date = d.minusDays(3), hours = BigDecimal(3L)))

        val found = repository.findWorkInTimeRange(d.minusWeeks(1), d)
        Assertions.assertEquals(2, found.count(), " not 2 items found for " + d)
        for (wi in found) { logger.info { wi } }
    }

    @Test
    fun queryDataFixtureTest() {

        val found = repository.findById(WorkItemTestData.ID_1)
        Assertions.assertTrue(found.isPresent)
        Assertions.assertEquals(WorkItemTestData.ID_1, found.get().id, " id 1 expected")
        Assertions.assertEquals(1, found.get().hours.toInt(), " hours")
        Assertions.assertNotNull( found.get().testAnnotation )
    }

}
