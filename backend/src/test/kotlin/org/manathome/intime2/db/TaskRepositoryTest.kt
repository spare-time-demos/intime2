package org.manathome.intime2.db

import com.tngtech.jgiven.Stage
import com.tngtech.jgiven.annotation.As
import com.tngtech.jgiven.annotation.ExtendedDescription
import com.tngtech.jgiven.annotation.Hidden
import com.tngtech.jgiven.junit5.SimpleScenarioTest
import mu.KLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.manathome.intime2.pd.Task
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.time.LocalDate
import javax.validation.Validation
import javax.validation.ValidationException

/**
 *  testing database access for tasks.
 *
 *   @see Task
 *   @author man@home
 */
@Suppress("RedundantModalityModifier")
@Tag("rest-api")
@DisplayName("task repository tests - task persistence")
@ExtendWith(SpringExtension::class)
@DataJpaTest
open class TaskRepositoryTest : SimpleScenarioTest<TaskRepositoryTest.TaskRepositoryTestSteps>() {
    companion object : KLogging()

    @Autowired
    lateinit var repository: TaskRepository

    /** spec. */
    @Test
    @Tag("feature:task-persistence")
    open fun scenario_persistence_create_task() {

        given()
                .a_task_with_name("SAMPLE-TASK")
                .a_repository(repository)
        `when`()
                .I_save_the_task()
        then()
                .I_can_find_the_task_searching_by_name("SAMPLE-TASK")
        then()
                .I_can_find_the_task_searching_from_until(
                        LocalDate.of(2018,1,11),
                        LocalDate.of(2018,1,30))
        then()
                .I_must_not_find_a_task_searching_by_name("OTHER-TASK")
    }

    /** spec. */
    @Test
    @Tag("feature:task-persistence")
    open fun scenario_persistence_create_task_with_time_range() {

        given()
                .a_task_with_name("SAMPLE-RANGED-TASK")
                .a_repository(repository)
        `when`()
                .I_specify_a_time_range_for_task(
                        LocalDate.of(2018,1,12),
                        LocalDate.of(2018,3,12))
        `when`()
                .I_save_the_task()
        then()
                .I_can_find_the_task_searching_by_name("SAMPLE-RANGED-TASK")
        then()
                .I_can_find_the_task_searching_from_until(
                        LocalDate.of(2018,1,11),
                        LocalDate.of(2018,1,30))
        then()
                .I_must_not_find_the_task_searching_from_until(
                        LocalDate.of(2018,3,13),
                        LocalDate.of(2018,3,17))
    }


    /** spec. */
    @Test
    @Tag("feature:task-persistence")
    open fun scenario_persistence_create_task_with_without_a_name() {

        given()
                .a_task_with_name("")
                .a_repository(repository)
        then()
                .I_can_not_save_the_task()
    }


    /** jgiven test steps. */
    open class TaskRepositoryTestSteps : Stage<TaskRepositoryTestSteps>() {

        private var task:       Task? = null
        private var repository: TaskRepository? = null

        @Hidden
        open fun a_repository(repository: TaskRepository?) : TaskRepositoryTestSteps {
            this.repository = repository
            return this
        }

        open fun a_task_with_name(name: String): TaskRepositoryTestSteps {

            this.task = Task(name = name)
            Assertions.assertNotNull(this.task)
            Assertions.assertEquals(name, this.task?.name)
            return this
        }

        open fun I_save_the_task(): TaskRepositoryTestSteps {

            val saved = this.repository?.saveAndFlush(this.task as Task)
            Assertions.assertNotNull(saved?.id)
            Assertions.assertNotEquals(0, saved?.id)
            this.task = saved

            return this
        }

        open fun I_can_not_save_the_task(): TaskRepositoryTestSteps {

            try {
                this.task!!.validate()
                this.repository!!.saveAndFlush(this.task as Task)
                Assertions.fail<Nothing>("save task $task should have failed.")

            }catch(ve: ValidationException) {
                // fall through
            }
            return this
        }


        open fun I_can_find_the_task_searching_by_name(name: String) : TaskRepositoryTestSteps {

            val foundTasks = repository?.findByName(name)?.toList()
            Assertions.assertTrue(foundTasks?.size ?: 0 > 0, "expected to find tasks with name $name in db.")
            Assertions.assertTrue(foundTasks?.all { it.name == name} ?: false, "expected to find only tasks with exakt name $name")

            Assertions.assertTrue(foundTasks?.any{ it.id == this.task?.id} ?: false, "expected to find task $task")

            return this
        }

        @As( "I can find the task searching from $ until $" )
        @ExtendedDescription("testing repository.findActiveTasks")
        open fun I_can_find_the_task_searching_from_until(from: LocalDate, until: LocalDate) : TaskRepositoryTestSteps {

            val foundTasks = repository?.findActiveTasks(from, until)?.toList()

            Assertions.assertTrue(foundTasks?.size ?: 0 > 0, "expected to find tasks within $from - $until")
            Assertions.assertTrue(foundTasks?.any{ it.id == this.task?.id} ?: false, "expected to find task $task")

            return this
        }

        @As( "I must not find the task searching from $ until $" )
        open fun I_must_not_find_the_task_searching_from_until(from: LocalDate, until: LocalDate): TaskRepositoryTestSteps {

            val foundTasks = repository?.findActiveTasks(from, until)?.toList()

            Assertions.assertFalse(foundTasks?.any{ it.id == this.task?.id} ?: false, "expected not to find task $task")

            return this
        }


        open fun I_must_not_find_a_task_searching_by_name(name: String) : TaskRepositoryTestSteps {

            val foundTasks = repository?.findByName(name)?.toList()
            Assertions.assertEquals(0, foundTasks?.size, "expected not to find tasks with name $name in db.")

            return this
        }

        open fun I_specify_a_time_range_for_task(from: LocalDate, until: LocalDate): TaskRepositoryTestSteps {

            this.task!!.activeFrom = from
            this.task!!.activeUntil = until

            return this
        }

    }
}
