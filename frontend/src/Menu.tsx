import * as React from 'react';
import { Link } from 'react-router-dom';
import * as intl from 'react-intl-universal';

const logo = require('./intime2_logo_small.svg');

/** intime2 main menu. */
export class Menu extends React.Component {

    render() {
        return (
            <div className="container-fluid">

                <div className="row">
                    <div className="col-12">
                        <div className="App-header">
                            <img src={logo} className="App-logo" alt="logo" />
                            <h2>{intl.get('APP_TITLE')}</h2>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
                            <a className="navbar-brand" href="#">inTime</a>

                            <button
                                className="navbar-toggler"
                                type="button"
                                data-toggle="collapse"
                                data-target="#collapsibleNavbar"
                            >
                                <span className="navbar-toggler-icon">T</span>
                            </button>

                            <div className="collapse navbar-collapse" id="collapsibleNavbar">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/">{intl.get('navigation_time_entry')}</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/reporting">
                                            {intl.get('navigation_reporting')}
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/tasks">
                                            {intl.get('navigation_tasks')}
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/projects">
                                            {intl.get('navigation_projects')}
                                        </Link>
                                    </li>
                                </ul>
                            </div>

                        </nav>
                    </div>
                </div>

                <div className="row">
                    <hr />
                </div>

            </div>
        );
    }
}
