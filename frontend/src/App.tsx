import * as React from 'react';
import track from 'react-tracking';
import { HashRouter as Router, Route } from 'react-router-dom';
import * as  Raven from 'raven-js';

import { language } from './actions/app-state-actions';
import store from './actions/store';

import { UrlUtil } from './service/url-util';
import { TraceService } from './service/trace-service';

import TimeEntryContainerPage from './time-entry/TimeEntryContainerPage';
import MessageDisplayContainer from './MessageDisplayContainer';
import ReportingContainerPage from './reporting/ReportingContainerPage';
import TasksContainerPage from './tasks/TasksContainerPage';
import { ProjectsPage } from './projects/ProjectsPage';
import { Menu } from './Menu';

import './App.css';
import { SUPPORTED_LOCALES, I18NSupport, locales } from './i18n';
import * as intl from 'react-intl-universal';
import Log from './util/Logging';

/**
 * inTime2 react application.
 *
 * for more documentation see https://spare-time-demos.gitlab.io/intime2/.
 *
 * @author man@home
 */
@track({}, { dispatch: (data) => App.forwardTracing(data) })
class App extends React.Component {

    /** forward tracing information to backend collected by react-tracing.. */
    public static forwardTracing(data: Object) {
        new TraceService().sendTrack(data).then(() => { /* nothing, put debugging in here */ });
    }

    constructor(props: Object) {
        super(props);
        this.onSelectChangeLocale = this.onSelectChangeLocale.bind(this);

        Raven.config('https://c8c45f9337524c11a7a30d1ad0e3c9ef@sentry.io/1205618')
             .install();
    }

    /** detect an set language data / using react-intl-universal. */
    loadLocales() {
        const currentLocale = I18NSupport.evaluateCurrentLocale();

        intl.init({
            currentLocale: currentLocale,
            locales,
        })
            .then(() => {
                this.setState({ initDone: true });
            });

        store.dispatch(language(currentLocale));

    }

    // tag::client-side-logging[]
    /** app fallback error handler. */
    componentDidCatch(error: Error, info: object) {

        Log('app did catch').fatalException(`${info}`, error);
        // end::client-side-logging[]
        // tag::sentry-logging[]
        Raven.captureException(error, { extra: info });
        // end::sentry-logging[]
    }

    componentDidMount() {
        Log('app').info(`logging enabled in app`);
        this.loadLocales();
    }

    /** user changes language. */
    onSelectChangeLocale(e: React.FormEvent<HTMLSelectElement>) {
        location.search = `?lang=${e.currentTarget.value}`; // change langauge, will be detected from int.init()
    }

    /** dropdown for language selection. */
    renderLocaleSelector() {
        const lng = store.getState().appState.language;

        Log('app').debug(`renderLocalSelector, currently with ${lng}`);
        return (
            <select onChange={e => this.onSelectChangeLocale(e)} value={lng}>
                {SUPPORTED_LOCALES.map(locale => (
                    <option key={locale.value} value={locale.value}>{locale.name}</option>
                ))}
            </select>
        );
    }

    /** render app. */
    render() {

        const baseUrl = new UrlUtil().buildRestUrl('info');
        const envMode = `env: ${process.env.REACT_APP_ENVIRONMENT} srv: ${baseUrl}`;

        return (
            <Router>
                <div className="App">

                    <Menu />

                    <div className="container">
                        <MessageDisplayContainer />
                    </div>

                    <div className="container">
                        <Route path="/reporting"        component={ReportingContainerPage} />
                        <Route path="/projects"         component={ProjectsPage} />
                        <Route path="/tasks"            component={TasksContainerPage} />
                        <Route path="/" exact={true}    component={TimeEntryContainerPage} />
                        <Route path="/entry/:date"      component={TimeEntryContainerPage} />
                    </div>

                    <footer className="footer center-on-small-only">
                        <div className="container-fluid">
                            © 2018 man@home mode: {envMode} language: {this.renderLocaleSelector()}
                        </div>
                    </footer>

                </div>
            </Router>
        );
    }

}

export default App;
