import { User } from './user';

describe('User', () => {

    it('should be used as plain object!', () => {
        const user = new User( 'dmy', 'dummy');
        expect(user.userId).toEqual('dmy');
    });
     
});
