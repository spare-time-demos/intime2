import * as moment from 'moment';

import { WorkWeek } from './work-week';
import { WorkItem } from './work-item';
import { Task } from './task';

describe('WorkWeek', () => {

    const startDate = moment('2018-02-05').toDate();

    let ww: WorkWeek;
    const d = moment().startOf('year').toDate();
    const t = new Task(1, 'task1');
    const t2 = new Task(2, 'task2');
    const monday = moment('2018-02-05').toDate();

    beforeEach( () => {
        ww = WorkWeek.createMockWeek(startDate)[0];
    });

    it('should be created', () => {
        expect(ww.task.name).toBe('task 1');
        expect(ww.id).toBe(1);
        expect(ww.startDate).toEqual(monday);
    });

    it('should recognize mid week starting tasks', () => {
        ww = WorkWeek.createMockWeek(startDate)[1];

        expect(ww.id).toBe(2);
        expect(ww.isActive(0)).toBeFalsy();
        expect(ww.isActive(1)).toBeTruthy();
        expect(ww.isActive(2)).toBeTruthy();
    });

    it('should convert to work-items', () => {
        const items = ww.toItems();
        expect(items.length).toBe(7);
        expect(items[0].hours).toBe(1);
        expect(items[1].hours).toBe(2);
        expect(items[5].hours).toBe(0);
        expect(items[6].hours).toBe(7);
        expect(items.every( i => i.task.id === 1 )).toBeTruthy();
        expect(items[0].date).toEqual(monday);
        expect(items[3].date.getDay()).toBe(moment(monday).add(3, 'days').toDate().getDay());
    });

    it('should convert from work-items', () => {
        const items = Array<WorkItem>(2);
        items[0] = new WorkItem(t, d, 7);
        items[1] = new WorkItem(t, moment(d).add(2, 'days').toDate(), 5);

        const nww = WorkWeek.buildFromItems(t, d, items);
        expect(nww.hours[0]).toBe(7);
        expect(nww.hours[1]).toBe(0);
        expect(nww.hours[2]).toBe(5);

        expect(nww.task.id).toBe(1);
        expect(nww.startDate.getDate()).toBe(d.getDate());

        expect(nww.toItems().length).toBe(7);
    });

    it('should check on wrong task in work-items', () => {
        const items = Array<WorkItem>(2);
        items[0] = new WorkItem(t, d, 7);
        items[1] = new WorkItem(t2, moment(d).add(2, 'days').toDate(), 5);
        try {
            const nww = WorkWeek.buildFromItems(t, d, items);
            fail(`exception wrong task expected in ${nww}`);
        } catch (e) {
            expect(e instanceof Error).toBeTruthy();
        }
    });

    it('should check on wrong date in work-items', () => {
        const items = Array<WorkItem>(1);
        items[0] = new WorkItem(t, moment(d).add(8, 'days').toDate(), 9);
        try {
            const nww = WorkWeek.buildFromItems(t, d, items);
            fail(`exception wrong task expected in ${nww}`);
        } catch (e) {
            expect(e instanceof RangeError).toBeTruthy();
        }
    });

    it('should build week with correct starting date', () => {
        const middleOfWeek = moment('2018-02-03').toDate(); // saturday
        const w = WorkWeek.buildFromAnyDate(new Task(3, 'no'), middleOfWeek);
        expect(moment(w.startDate).toDate()).toEqual(moment('2018-01-29').toDate()); // monday
    });

    it('should set hours updated for correct date', () => {

        const middleOfWeek = moment('2018-02-03').toDate(); // saturday
        const w = WorkWeek.buildFromAnyDate(new Task(4, 'no'), middleOfWeek);

        w.setHour(middleOfWeek, 4);
        w.setHour(moment('2018-02-01').toDate(), 2);
        expect(w.hours[5]).toBe(4); // saturday
        expect(w.hours[3]).toBe(2); // thursday
        expect(w.hours[4]).toBe(0); // friday not set.
    });

    it('should set and get hours on correct date, ignoring hours', () => {
        const middleOfWeek = moment('2018-02-03').toDate(); // saturday
        const w = WorkWeek.buildFromAnyDate(new Task(4, 'no'), middleOfWeek);

        w.setHour(middleOfWeek, 9); // overwritten by( next line)
        w.setHour(moment(middleOfWeek).add(12, 'hours').toDate(), 5);
        w.setHour(moment(middleOfWeek).add(25, 'hours').toDate(), 6); // next day
        w.setHour(moment(middleOfWeek).subtract(1, 'minutes').toDate(), 4); // previous day

        expect(w.hours[5]).toBe(5); // saturday
        expect(w.hours[6]).toBe(6); // sunday
        expect(w.hours[4]).toBe(4); // friday

        expect(w.getHour(middleOfWeek)).toBe(5);
        expect(w.getHour(moment(middleOfWeek).add(25, 'hours').toDate())).toBe(6); // next day
    });

    it('should get correct date from index', () => {
        const firstDayOfWeek = moment('2018-02-05').toDate(); // monday
        const w = WorkWeek.buildFromAnyDate(new Task(5, 'monday'), firstDayOfWeek);

        expect(w.getDateOf(0)).toEqual(firstDayOfWeek); // saturday
        expect(w.getDateOf(1)).toEqual(moment('2018-02-06').toDate()); // tuesday
        expect(w.getDateOf(4)).toEqual(moment('2018-02-09').toDate()); // friday
    });

});
