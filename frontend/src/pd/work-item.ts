import { Task } from './task';

import * as moment from 'moment';

/** n working hours at a given date worked on a given task.
 * (unique on taskId+date) .
 */
export class WorkItem {

    task:       Task;
    date:       Date;
    hours:      number;

    /** cope with json deserializer deficiencies */
    // tslint:disable-next-line:no-any
    public static fromJson(jsonObject: any): WorkItem {

        const t = Task.fromJson(jsonObject.task);
        return new WorkItem(t, new Date(jsonObject.date), jsonObject.hours);
    }

    constructor(task: Task, date: Date, hours: number) {
        this.task = task;
        this.date = date;
        this.hours = hours;

        this.date.toJSON = function() { return moment(this).format('YYYY-MM-DD'); }; // date only json.
    }

    public toString(): string {
        return `item[${this.date}, ${this.hours}h, task: ${this.task.name}]`;
    }
}
