import { Task } from './task';
import * as moment from 'moment';
// import Log from '../util/Logging';
// import { LogInit } from '../util/Logging';

describe('Task', () => {

    it('should be used as plain object!', () => {
        const t = new Task( 1, 'dummy');
        expect(t.activeFrom).toBeNull();
    });

    it('should be deserializable from json', () => {

        const from  = moment('2018-01-02').format('YYYY-MM-DD');
        const until = moment('2018-02-03').format('YYYY-MM-DD');
        const json = JSON.parse(`{"id":1, "name": "xy", "activeFrom":"${from}", "activeUntil":"${until}"}`);

        const t = Task.fromJson(json);

        expect(t.id).toEqual(1);
        expect(t.name).toEqual('xy');
        expect(moment(t.activeFrom!).format('YYYY-MM-DD')).toEqual(from);
        expect(moment(t.activeUntil!).format('YYYY-MM-DD')).toEqual(until);
    });

    it('should serializable to json', () => {

        const t = new Task(3, 'my-name', moment('2018-02-03').toDate());
        const s = JSON.stringify(t);

        expect(s).toEqual('{"id":3,"name":"my-name","activeFrom":"2018-02-03","activeUntil":null}');
    });

    it('should calc isActive based on from/until', () => {

        const from  = moment('2018-01-02').toDate();
        const until = moment('2018-02-03').toDate();

        const t1 = new Task(1, 'calc-check', from, until);

        expect(t1.isActive(from)).toBeTruthy();
        expect(t1.isActive(until)).toBeTruthy();
        expect(t1.isActive(moment(until).add(1, 'day').toDate())).toBeFalsy();
        expect(t1.isActive(moment(from).subtract(1, 'day').toDate())).toBeFalsy();

        const t2 = new Task(2, 'calc-check', from);

        expect(t2.isActive(moment(until).add(1, 'day').toDate())).toBeTruthy();
        expect(t2.isActive(moment(from).subtract(1, 'day').toDate())).toBeFalsy();

    });

});
