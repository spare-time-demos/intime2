import { Task } from './task';
import { WorkItem } from './work-item';
import { WorkWeek } from './work-week';

export class WorkWeekBuilder {

    /** build week from up to 7 day items. */
    public static buildWeekFromTasksAndItems(
        startDate: Date,
        tasks: Task[],
        items: WorkItem[]): WorkWeek[] {

        return  tasks.map( t =>
                    WorkWeek.buildFromItems(t,
                                            startDate,
                                            items.filter(i => i.task.id === t.id))
                    );
    }

}
