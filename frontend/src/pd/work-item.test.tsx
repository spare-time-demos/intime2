import * as moment from 'moment';

import { WorkItem } from './work-item';
import { Task } from './task';

describe('WorkItem', () => {

    let wi: WorkItem;

    beforeEach(() => {
        const day = moment().startOf('week').add(3, 'days').toDate();
        const t = new Task(1, 'a task');

        wi = new WorkItem(t, day, 5);
    });

    it('should be used as plain object!', () => {

        expect(wi.task.id).toBe(1);
    });

    it('should be serializable to json', () => {
        const json = JSON.stringify(wi);
        expect(json).toBeTruthy();
    });

    it('should be deserializable from json', () => {
        const json = '{"task":{"id":3,"name":"a task"},"date":"2017-10-19T01:00:00.000Z","hours":5}';
        const readWorkItem = WorkItem.fromJson(JSON.parse(json));

        expect(readWorkItem instanceof WorkItem).toBeTruthy();
        expect(readWorkItem).toBeTruthy();
        expect(readWorkItem.task.id).toBe(3);
        expect(readWorkItem.hours).toBe(5);
        expect(readWorkItem.date).toBeTruthy();
        expect(readWorkItem.date.getFullYear()).toBe(2017);
        expect(readWorkItem.date.getMonth()).toBe(10 - 1);
        expect(readWorkItem.date.getDate()).toBe(19);
    });

    it('should be deserializable from java backend json', () => {
        const json = '[{"id":4,"date":[2017,11,2],"hours":3.00,"task":{"id":6,"name":"dummy"}}]';
        const readWorkItems: Object[] = JSON.parse(json);
        // console.log(`java provided json parsed: ${json}`);
        const items: WorkItem[] = readWorkItems.map(i => WorkItem.fromJson(i));
        // console.log(`converted: ${items.length}`);
        expect(items.length).toBe(1);
        expect(items[0] instanceof WorkItem).toBeTruthy();
        expect(items[0].hours).toBe(3);
        expect(items[0].date.getFullYear()).toBe(2017);
        expect(items[0].task instanceof Task).toBeTruthy();
        expect(items[0].task.id).toBe(6);
    });

});
