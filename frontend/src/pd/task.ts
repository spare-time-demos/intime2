import * as moment from 'moment';
/** working task */
export class Task {

    /** pk. */
    id:     number;

    /** task name. */
    name:   string;

    /** if set: working hours editing only valid from this date */
    activeFrom:  Date | null;

    /** if set: working hours editing only valid until this date. */
    activeUntil: Date | null;

    /** cope with json deserializer deficiencies */
    // tslint:disable-next-line:no-any
    public static fromJson(jsonObject: any): Task {
        return new Task(
            jsonObject.id,
            jsonObject.name,
            jsonObject.activeFrom  ? new Date(jsonObject.activeFrom)  : null,
            jsonObject.activeUntil ? new Date(jsonObject.activeUntil) : null
        );
    }

    constructor(id: number, name: string, activeFrom: Date | null = null, activeUntil: Date | null = null) {
        this.id = id;
        this.name = name;
        this.activeFrom  = activeFrom  == null ? null : new Date(activeFrom.getTime());
        this.activeUntil = activeUntil == null ? null : new Date(activeUntil.getTime());
    }

    toJSON(): Object {

        if (this.activeFrom != null) {
            this.activeFrom.toJSON = function() { return moment(this).format('YYYY-MM-DD'); }; // date only json.
        }
        if (this.activeUntil != null) {
            this.activeUntil.toJSON = function() { return moment(this).format('YYYY-MM-DD'); }; // date only json.
        }

        return this;
    }

    /** check from/until time range. */
    public isActive(date: Date): boolean {

        return (this.activeFrom == null  || this.activeFrom <= date) &&
               (this.activeUntil == null || this.activeUntil >= date);
    }
}
