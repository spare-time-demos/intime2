/** a project definition. */
export class Project {
    id: number;
    name: string;
}