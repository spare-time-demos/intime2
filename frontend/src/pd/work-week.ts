import * as moment from 'moment';

import { Task } from './task';
import { WorkItem } from './work-item';
import { DateFormat } from '../util/DateFormat';

/** aggregates 7 days (a week) of work for a given task. */
export class WorkWeek {

    id:         number;
    task:       Task;
    startDate:  Date;
    hours:      number[] = [0, 0, 0, 0, 0, 0, 0];

    /** build week from up to 7 items. */
    public static buildFromItems(
        task: Task,
        startDate: Date,
        items: WorkItem[]): WorkWeek {

        const ww = this.buildFromAnyDate(task, startDate);

        for (const item of items) {
            const offsetInDays = moment(item.date).diff(startDate, 'days');
            if (offsetInDays < 0 || offsetInDays > 7) {
                throw RangeError(
                    `buildFromItems: item date ${item.date.toLocaleDateString()}` +
                    `not in week ${ww.startDate.toLocaleDateString()}`);
            }
            if (item.task.id !== task.id) {
                throw Error(`item task ${task.id} differs from week ${ww.task.id}`);
            }
            ww.hours[offsetInDays] = item.hours;
        }
        return ww;
    }

    public static buildFromAnyDate(task: Task, anyDateInWeek: Date): WorkWeek {
        const startDate = DateFormat.startOfWeek(anyDateInWeek);
        return new WorkWeek(task, startDate);
    }

    public static findByTask(workWeek: WorkWeek[], taskId: number): WorkWeek {
        const found = workWeek.find( w => w.task.id === taskId);
        if (!found) {
            throw `no task ${taskId} in week found`;
        }
        return found;
    }

    /** test code: mock data creation. */
    public static createMockWeek(startDay: Date): WorkWeek[] {

        const date = startDay; // moment('2018-02-05').toDate();
        const nextDate = new Date(moment(startDay).add(1, 'day').toDate().getTime());

        const mockWeek = [
            WorkWeek.buildFromAnyDate(new Task(1, 'task 1'), date),
            WorkWeek.buildFromAnyDate(new Task(2, 'another task 2', nextDate), date),
            // variable test part!
            WorkWeek.buildFromAnyDate(new Task(3, `a last task 3 from ${date.toDateString()}`), date)
        ];
        mockWeek[0].setHour(moment(date).add(0, 'days').toDate(), 1);
        mockWeek[0].setHour(moment(date).add(1, 'days').toDate(), 2);
        mockWeek[0].setHour(moment(date).add(2, 'days').toDate(), 3);
        mockWeek[0].setHour(moment(date).add(6, 'days').toDate(), 7);
        mockWeek[1].setHour(moment(date).add(3, 'days').toDate(), 4);
        // variable test part!
        mockWeek[1].setHour(moment(date).add(6, 'days').toDate(), date.getDate() % 12);
        return mockWeek;
    }

    /** .ctor, date must be start of week. */
    constructor(task: Task, startDate: Date) {

        if (task == null || task.id === null) {
            throw 'no task or task.id for WorkWeek';
        }

        this.task = task;
        this.startDate = moment(startDate)
                               .startOf('day')
                               .toDate();
        this.id = task.id;
    }

    /** is given date within week. */
    public isInWeek(date: Date): boolean {
        return moment(DateFormat.startOfWeek(date)).isSameOrAfter(this.startDate) &&
               moment(this.startDate).endOf('isoWeek').isSameOrAfter(date);
    }

    /** change hours for given day in week. */
    public setHour(date: Date, newHours: number) {
        if ( !this.isInWeek(date)) {
            throw `setHour: ${newHours} for ${date} not in week ${this.startDate}`;
        }

        const dayDiff = moment(date).startOf('day').diff(moment(this.startDate), 'days');
        if (dayDiff < 0 || dayDiff > 6) {
            throw `setHour: Implementation Error: ${dayDiff} Out of Week Days ${this.startDate} `;
        }

        this.hours[dayDiff] = newHours;
    }

    /** change hours for given day in week. */
    public getHour(date: Date): number {
        if ( !this.isInWeek(date)) {
            throw `getHour: requested for ${date} not in week ${this.startDate}`;
        }
        const dayDiff = moment(date).startOf('day').diff(moment(this.startDate), 'days');
        return this.hours[dayDiff];
    }

    /** get date for given week index. */
    public getDateOf(index: number): Date {
        if (index > 6) {
            throw `getDateOf: not a valid hour index in week: ${index}`;
        }
        return moment(this.startDate).add(index, 'days').toDate();
    }

    /** is task for given day index of week active. */
    public isActive (index: number): boolean {

        return this.task.isActive(this.getDateOf(index));
    }

    /** first day in this week. */
    public getStartDate(): Date {
        return this.startDate;
    }

    /** sum of hours. */
    public sum(): number {
        return this.hours.reduce( (a, b) => a + b, 0);
    }

    public toItems(): WorkItem[] {
        const ww = new Array<WorkItem>(7);
        for (let i = 0; i <= 6; i++) {
            ww[i] = new WorkItem(
                this.task,
                moment(this.startDate).add(i, 'days').toDate(),
                this.hours[i]
            );
        }
        return ww;
    }

    public toString(): string {
        return `ww[${this.task.name}, ${this.startDate.toLocaleDateString()}: ${this.sum()}h]`;
    }
}
