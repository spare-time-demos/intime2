import * as moment from 'moment';

import { WorkItem } from './work-item';
import { Task } from './task';
import { WorkWeekBuilder } from './work-week-builder';

describe('WorkWeekBuilder', () => {

    it('should build week from tasks and items', () => {
        const t1 = new Task(10, 'task10');
        const t2 = new Task(20, 'task20');
        const t3 = new Task(30, 'task30 only');

        const d = new Date('2018-03-12');

        const items = Array<WorkItem>(3);
        items[0] = new WorkItem(t1, d, 7);
        items[1] = new WorkItem(t2, moment(d).add(1, 'days').toDate(), 5);
        items[2] = new WorkItem(t2, moment(d).add(2, 'days').toDate(), 4);
        items[3] = new WorkItem(t2, moment(d).add(3, 'days').toDate(), 3);

        const nww = WorkWeekBuilder.buildWeekFromTasksAndItems(d, [t1, t2, t3], items);

        expect(nww.length).toBe(3); // 3 tasks
        expect(nww[0].sum()).toBe(7);
        expect(nww[1].task.id).toBe(20);
        expect(nww[1].sum()).toBe(12); // 5+4+3
        expect(nww[2].task.id).toBe(30);
        expect(nww[2].sum()).toBe(0); // no items
    });

});
