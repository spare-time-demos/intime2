import * as React from 'react';

import * as moment from 'moment';
import * as intl from 'react-intl-universal';

interface DateHeaderProps {
  date: Date;
}

/** ui: column headers with dates per day in week. */
export class DateHeader extends React.Component<DateHeaderProps> {

    private days: number[] = [0, 1, 2, 3, 4, 5, 6];

    /** return day with offset for ui display */
    addDays(start: Date, incDays: number): Date {
      const newDay = moment(start).clone().add(incDays, 'days');
      return newDay.toDate();
    }

    render() {
        return(
        <div className="d-none d-md-block">
            <div className="row">
              <div className="col-3">&nbsp;</div>
              {this.days.map(( day: number, index: number ) => {
                    return(
                      <div className="col" key={index}>
                        <small className="d-none d-lg-block" data-test="day-header">
                          {intl.get('time_entry_day', {day: this.addDays(this.props.date, day)})}
                        </small>
                      </div>);
              })}
              <div className="col">
                <small>sum</small>
                <small>&nbsp;</small>
              </div>
              <div className="col">
                <small>&nbsp;</small>
                <small>&nbsp;</small>
              </div>
            </div>
        </div>
        );
    }
}
