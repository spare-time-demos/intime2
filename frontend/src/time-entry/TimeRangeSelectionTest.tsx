import * as React from 'react';
import { shallow } from 'enzyme';
import { TimeRangeSelection } from './TimeRangeSelection';

describe('UI TimeRangeSelection', () => {

    let startDate = new Date(2018, 0, 1);
    let nextDate = new Date(2018, 0, 7);

    it('change 1.1.2018 to 7.1 with click', () => {
        const onDateChangeHandler = jest.fn();
        const wrapper =
            shallow(<TimeRangeSelection date={startDate} onDateChange={onDateChangeHandler} tracking={undefined}/>);
        const button = wrapper.find('#next');
        button.simulate('click');
        expect(onDateChangeHandler).toHaveBeenCalledTimes(1);
        expect(onDateChangeHandler.mock.calls[0][0]).toEqual(nextDate);
    });

});
