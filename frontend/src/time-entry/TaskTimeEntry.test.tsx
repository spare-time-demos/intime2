import * as React from 'react';
import { shallow } from 'enzyme';
import { TaskTimeEntry } from './TaskTimeEntry';
import { WorkWeek } from '../pd/work-week';

import * as renderer from 'react-test-renderer';
import * as moment from 'moment';

describe('UI TaskTimeEntry', () => {

    const startDate = moment('2018-02-05').toDate();
    let workWeek: WorkWeek;

    beforeEach(() => {
        workWeek = WorkWeek.createMockWeek(startDate)[0];
    });

    it('working hour sum is displayed on badge', () => {
        let fn = jest.fn();
        const wrapper = shallow(<TaskTimeEntry workWeek={workWeek} onHourChange={fn} />);
        const sumBadge = wrapper.find('[data-test="sum"]');
        expect(sumBadge).toHaveLength(1);
        expect(sumBadge.first().html()).toMatch(workWeek.sum().toString());
    });

    it('mid week starting task has some inputs disabled', () => {
        const midWeek = WorkWeek.createMockWeek(startDate)[1];
        let fn = jest.fn();
        const wrapper = shallow(<TaskTimeEntry workWeek={midWeek} onHourChange={fn} />);
        const input = wrapper.find('input').first();
        expect(input.prop('disabled')).toBeTruthy();
        const lastInput = wrapper.find('input').last();
        expect(lastInput.prop('disabled')).toBeFalsy();
    });

    it('changing hours triggers callback', () => {
        let fn = jest.fn();
        const wrapper = shallow(<TaskTimeEntry workWeek={workWeek} onHourChange={fn} />);
        const inputs = wrapper.find('input');
        expect(inputs).toHaveLength(7);
        const input = inputs.first();

        input.simulate('change',  {target: {value: '6'}});
        input.simulate('change',  {target: {value: '7'}});
        expect(fn.mock.calls.length).toBe(2); // 2 calls

        expect(fn.mock.calls[1][0]).toEqual(workWeek.task.id);
        expect(fn.mock.calls[1][1]).toEqual(workWeek.startDate);
        expect(fn.mock.calls[1][2]).toEqual(7);
    });

    it('should create a html line with task hours (snapshot)', () => {
        let fn = jest.fn();

        const component = renderer.create(
            <TaskTimeEntry workWeek={workWeek} onHourChange={fn} />,
          );

        const json = component.toJSON();
        expect(json).toMatchSnapshot();
    });

});
