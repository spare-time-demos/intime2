import * as React from 'react';
import { WorkWeek } from '../pd/work-week';

export interface TaskTimeEntryProps {
    workWeek: WorkWeek;
    onHourChange: (taskId: number, day: Date, newHours: number) => void;
}

/** ui: one week entry line for one given task (editing one workWeek). */
export class TaskTimeEntry extends React.Component<TaskTimeEntryProps> {

    private days: number[] = [1, 2, 3, 4, 5, 6, 7];

    constructor(props: TaskTimeEntryProps) {
        super(props);
    }

    handleHourChange(index: number, newValue: string) {
        const newHour = parseFloat(newValue);
        this.props.onHourChange(this.props.workWeek.task.id,
                                this.props.workWeek.getDateOf(index),
                                newHour);
    }

    render() {
        return(
            <div className="row">
              <div className="col-3">{this.props.workWeek.task.name}</div>
              {this.days.map(( day: number, index: number ) => {
                    return(
                    <div className="col-1 d-none d-md-block" key={index}>
                        <input
                              className="form-control"
                              placeholder="hours"
                              data-test="hourInput"
                              value={this.props.workWeek.hours[index]}
                              type="number"
                              onChange={(e) => { this.handleHourChange(index, e.target.value); }}
                              min="0"
                              max="24"
                              disabled={!this.props.workWeek.isActive(index)}
                        />
                    </div>
                    );
              })}
              <div className="col-1 d-none d-md-block">
                <span className="badge badge-secondary" data-test="sum">{this.props.workWeek.sum()}</span>
              </div>
              <div className="col-1 d-none d-md-block">
                &nbsp;
              </div>
            </div>
        );
    }
}
