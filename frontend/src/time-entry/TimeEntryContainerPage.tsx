import { connect, Dispatch } from 'react-redux';
import { changeHours, WeekActions, saveWeek, loadAsyncWeek } from '../actions/work-week-actions';

import { TimeEntry } from './TimeEntry';
import { StoreRoot } from '../actions/redux-reducers';
import { WorkWeek } from '../pd/work-week';

export function mapStateToProps(store: StoreRoot) {
    return {
        workWeeks: store.workWeeks,
        date: store.date
    };
}

export function mapDispatchToProps(dispatch: Dispatch<WeekActions>) {
    return {
        // editing current week
        onHourChange: (taskId: number, day: Date, hour: number) => {
            dispatch(changeHours(taskId, day, hour));
        },
        // change selected week
        onDateChange: (date: Date) => {
            dispatch(loadAsyncWeek(date));
        },
        onSaveWeek: (week: WorkWeek[]) => {
            dispatch(saveWeek(week));
        }
    };
}

const TimeEntryContainerPage = connect(mapStateToProps, mapDispatchToProps)(TimeEntry);

/** time entry page. */
export default TimeEntryContainerPage;
