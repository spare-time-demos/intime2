import * as React from 'react';

export interface SumFooterProps {
  sumPerDay: number[];
}

/** render a footer with daily working hours. */
export class SumFooter extends React.Component<SumFooterProps> {
    
    constructor(props: SumFooterProps) {
      super(props);
    }

    render() {
        return(
        <div className="d-none d-md-block">
            <div className="row">
              <div className="col-3">&nbsp;</div>
              {this.props.sumPerDay.map(( sumOfDay: number, index: number ) => {
                    return( 
                      <div className="col" key={index}>
                        <span className="badge badge-secondary" data-test="sumPerDay">{sumOfDay}</span>
                      </div>);
              })}
              <div className="col">
                <small>&nbsp;</small>
              </div>
              <div className="col">
                <small>&nbsp;</small>
                <small>&nbsp;</small>
              </div>
            </div>
        </div>        
        );        
    }
}
