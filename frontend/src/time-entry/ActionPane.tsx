import * as React from 'react';
import * as intl from 'react-intl-universal';

export interface ActionPaneProps {
    onSave: () => void;
}

/** save button for now. */
export class ActionPane extends React.Component<ActionPaneProps> {

    constructor(props: ActionPaneProps) {
      super(props);
    }

    handleOnSave() {
        this.props.onSave();
    }

    render() {
        return(
            <div className="row">
                <div className="col-12">
                    <button type="button" id="save" className="btn btn-default" onClick={(e) => this.handleOnSave()}>
                    {intl.get('time_entry_save')}
                    </button>
                </div>
            </div>
        );
    }
}
