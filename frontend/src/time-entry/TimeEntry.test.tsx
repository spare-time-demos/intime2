import * as React from 'react';
import { shallow } from 'enzyme';
import { TimeEntry } from './TimeEntry';
import { TaskTimeEntry } from './TaskTimeEntry';
import { WorkWeek } from '../pd/work-week';

import * as moment from 'moment';
import Log from '../util/Logging';
import * as H from 'history';

/// import track from 'react-tracking';

describe('TimeEntry', () => {

    const startDate = moment('2018-02-05').toDate();
    let workWeeks: WorkWeek[];
    let date: Date;

    beforeEach(() => {
        workWeeks = WorkWeek.createMockWeek(startDate);
        date = workWeeks[0].startDate;
    });

    it('each task gets a row', () => {

        Log().debug('testing TimeEntry');

        const fn1 = jest.fn();
        const fn2 = jest.fn();
        const fn3 = jest.fn();

        const location = H.createLocation('x');
        const history = H.createBrowserHistory();

        const wrapper = shallow(
            <TimeEntry
                date={date}
                workWeeks={workWeeks}
                onHourChange={fn1}
                onDateChange={fn2}
                onSaveWeek={fn3}
                match={{params: {date: '2017-01-03'}, isExact: true, path: '', url: ''}}
                location={location}
                history={history}
                staticContext={undefined}

            />).dive();

        expect(wrapper.find(TaskTimeEntry).length).toBe(workWeeks.length);
    });

});
