import * as React from 'react';
import { shallow } from 'enzyme';
import * as renderer from 'react-test-renderer';
import { ActionPane } from './ActionPane';

describe('UI ActionPane', () => {

    it('action pane with save button matches snapshot', () => {
        const onSaveHandler = jest.fn();
        const component = renderer.create(
            <ActionPane onSave={onSaveHandler} />,
          );

        const json = component.toJSON();
        expect(json).toMatchSnapshot();
    });

    it('action pane save click callback get executed.', () => {
        const onSaveHandler = jest.fn();
        const wrapper = shallow(<ActionPane onSave={onSaveHandler}/>);
        const button = wrapper.find('#save');
        button.simulate('click');
        expect(onSaveHandler).toHaveBeenCalledTimes(1); 
    });    
    
});
