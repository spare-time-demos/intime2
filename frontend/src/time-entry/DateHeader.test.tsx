import * as React from 'react';
import { shallow } from 'enzyme';
import { DateHeader } from './DateHeader';
import * as renderer from 'react-test-renderer';
import * as intl from 'react-intl-universal';
import { locales } from '../i18n';

describe('UI DateHeader', () => {

    let startDate = new Date(2018, 0, 1);

    intl.init({ locales, currentLocale: 'en-US' });

    it('create a date header for current week', async () => {

        const wrapper = shallow(<DateHeader date={startDate} />);
        const headers = wrapper.find('[data-test="day-header"]');
        expect(headers.length).toBe(7);
        expect(headers.first().text()).toEqual(expect.stringContaining('1/1/2018'));
        expect(headers.last().text()).toEqual(expect.stringContaining('1/7/2018'));
    });

    it('date header matches snapshot', async () => {

        const component = renderer.create(
            <DateHeader date={startDate} />,
        );

        const json = component.toJSON();
        expect(json).toMatchSnapshot();
    });

});
