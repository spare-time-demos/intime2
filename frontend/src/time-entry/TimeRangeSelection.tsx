import * as React from 'react';
import * as moment from 'moment';
import track, { TrackingProp } from 'react-tracking';
import * as intl from 'react-intl-universal';

interface TimeRangeSelectionProps {
    date: Date;
    onDateChange: (newDay: Date) => void;
    tracking: TrackingProp | undefined;
}

@track({ component: 'TimeRangeSelection' })
export class TimeRangeSelection extends React.Component<TimeRangeSelectionProps> {

    handleOnClick(daysDiff: number) {
        const newDate = moment(this.props.date).add(daysDiff, 'days').toDate();

        if ( this.props.tracking) {
            this.props.tracking.trackEvent({
                action: 'dateChange',
                data: newDate,
            });
        }
        this.props.onDateChange(newDate);
    }

    render() {
        return (
            <div className="row">
                <div className="col-12 d-none d-md-block">
                    <button type="button" id="prev" className="btn btn-light" onClick={(e) => this.handleOnClick(-7)}>
                        -
                    </button>
                    {intl.get('time_entry_selected_day', {day: this.props.date})}
                    <button type="button" id="next" className="btn btn-light" onClick={(e) => this.handleOnClick(+7)}>
                        +
                    </button>
                </div>
                <div className="col-12 d-md-none">
                    <button type="button" className="btn btn-light" onClick={(e) => this.handleOnClick(-1)}>
                        -
                    </button>
                    <button type="button" className="btn btn-light" onClick={(e) => this.handleOnClick(+1)}>
                        +
                    </button>
                </div>
            </div>
        );
    }
}
