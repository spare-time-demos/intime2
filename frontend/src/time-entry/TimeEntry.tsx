import * as React from 'react';

import { TimeRangeSelection } from './TimeRangeSelection';
import { DateHeader } from './DateHeader';
import { TaskTimeEntry } from './TaskTimeEntry';
import { WorkWeek } from '../pd/work-week';
import { SumFooter } from './SumFooter';
import { ActionPane } from './ActionPane';
import track from 'react-tracking';
import { RouteComponentProps } from 'react-router';
import Log from '../util/Logging';
import { DateFormat } from '../util/DateFormat';

/** official url path params possible */
interface PathParameter {
    /** iso date string yyyy-mm-dd. */
    date: string;
}

interface TimeEntryProps  extends RouteComponentProps<PathParameter> {
  date: Date;
  workWeeks: WorkWeek[];
  onDateChange: (date: Date) => void;
  onHourChange: (taskId: number, day: Date, newHours: number) => void;
  onSaveWeek: (workWeeks: WorkWeek[]) => void;
}

/** page with one weeks of tasks and their hour entry, time range change&display. */
@track({ page: 'TimeEntry'}, { dispatchOnMount: true } )
export class TimeEntry extends React.Component<TimeEntryProps> {

    public onDateChange(date: Date) {
        this.props.onDateChange(date);
        this.props.history.push(`/entry/${DateFormat.formatIso(date)}`);
    }

    /** honor url date (../entry/2018-01-03) string if possible for week selection. */
    handDateFromPath(dateString: string) {
        Log('TimeEntry').info(`query param date= ${dateString}`);

        let currentDate = this.props.date;
        if ( dateString && DateFormat.parseIsoFormat(dateString) != null) {
            const newStartDateFromPath = DateFormat.startOfWeek(DateFormat.parseIsoFormat(dateString)!);
            if ( newStartDateFromPath.getTime() !== currentDate.getTime() ) {
                currentDate = newStartDateFromPath;
            }
        }

        if ( currentDate.getTime() !== this.props.date.getTime() ||
             this.props.workWeeks.length === 0 ) {
             this.onDateChange(currentDate);
        }
    }

    constructor(props: TimeEntryProps) {
        super(props);
        this.onDateChange = this.onDateChange.bind(this);
        this.handDateFromPath(props.match.params.date);
    }

    @track((props: TimeEntryProps) => ({ action: 'saveWeek', data: props.date }))
    public handleSaveWeek() {
      this.props.onSaveWeek(this.props.workWeeks);
    }

    render() {

      return (
        <div>
            <TimeRangeSelection date={this.props.date} onDateChange={this.onDateChange} tracking={undefined}/>
            <DateHeader date={this.props.date}/>
            {this.props.workWeeks.map( ww =>
              <TaskTimeEntry key={ww.id} workWeek={ww} onHourChange={this.props.onHourChange} />
            )}
            <SumFooter sumPerDay={this.sumPerDay(this.props.workWeeks)}/>
            <br />
            <ActionPane onSave={() => {this.handleSaveWeek(); }} />
        </div>
      );
    }

    private sumPerDay(ww: WorkWeek[]): number[] {

      const sums = new Array<number>(6);
      for (let i = 0; i < 7; i++) {
        sums[i] = ww.map(w => w.hours[i]).reduce( (a, b) => a + b, 0);
      }
      return sums;
    }

}
