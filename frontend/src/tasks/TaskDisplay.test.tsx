import * as React from 'react';

import { Task } from '../pd/task';
import * as moment from 'moment';
import { TaskDisplay } from './TaskDisplay';
import * as renderer from 'react-test-renderer';
import { locales } from '../i18n';
import * as intl from 'react-intl-universal';

describe('UI TaskDisplay', () => {

    let task = new Task(1, 'test task with from', moment('2018-09-07').toDate(), null);

    it('display id and name', () => {

        intl.init({ locales, currentLocale: 'en-US' });
        const fn1 = jest.fn();

        const json = renderer.create(
                <TaskDisplay task={task} onTaskSelected={fn1} />).toJSON();

        expect(json).toMatchSnapshot();

    });

});
