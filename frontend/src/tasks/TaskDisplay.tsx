import * as React from 'react';

import { Task } from '../pd/task';
import * as intl from 'react-intl-universal';

import './TaskDisplay.css';

export interface TaskDisplayProps {
    task: Task;
    onTaskSelected: (task: Task) => void;
}

/** ui: on task line displayed. */
export class TaskDisplay extends React.Component<TaskDisplayProps> {

    constructor(props: TaskDisplayProps) {
        super(props);
    }

    render() {
        const t = this.props.task;
        return (
            <div className="row">
                <div className="col-1">
                    {t.id}
                </div>
                <div className="col-6">
                    {t.name}
                </div>
                <div className="col-2">
                    {t.activeFrom != null ? intl.get('date', { day: t.activeFrom }) : '*'}
                    -
                    {t.activeUntil != null ? intl.get('date', { day: t.activeUntil }) : '*'}
                </div>
                <div className="col-1">
                    <button
                        type="button"
                        id="save"
                        className="btn btn-default btn-sm with-margin"
                        onClick={(e) => this.props.onTaskSelected(t)}
                    >
                        {intl.get('task_select')}
                    </button>
                </div>
            </div>
        );
    }
}
