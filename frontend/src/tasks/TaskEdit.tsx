import * as React from 'react';

import { Task } from '../pd/task';
import * as intl from 'react-intl-universal';
import { TaskNameInput } from './TaskNameInput';
import { DateFormat } from '../util/DateFormat';

export interface TaskEditProps {
    task: Task;
    onTaskSave: (task: Task) => void;
    onTaskEdited?: (task: Task) => void;
    onTaskDelete?: (task: Task) => void;
}

export interface TaskEditState {
    task?: Task;
}

/** ui: on task line displayed */
export class TaskEdit extends React.Component<TaskEditProps, TaskEditState> {

    static getDerivedStateFromProps(nextProps: TaskEditProps, prevState: TaskEditState): TaskEditState | null {
        if (!prevState || prevState.task!.id !== nextProps.task.id) {
            return { task: Object.assign(nextProps.task) };
        }

        return null;         // Return null to indicate no change to state.
    }

    constructor(props: TaskEditProps) {
        super(props);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
    }

    onNameChange(newName: string) {
        const t = this.state.task;
        if (t) {
            t.name = newName;
            this.setState({ task: t });
            if (this.props.onTaskEdited) {
                this.props.onTaskEdited(t);
            }
        }
    }

    onDateChange(field: string, newDate: string) {
        const t = this.state.task;
        if (t) {
            if (field.startsWith('activeFrom')) {
                t.activeFrom = DateFormat.parseIsoFormat(newDate);
            } else {
                t.activeUntil = DateFormat.parseIsoFormat(newDate);
            }
            this.setState({ task: t });
            if (this.props.onTaskEdited) {
                this.props.onTaskEdited(t);
            }
        }
    }

    render() {
        let t = this.state.task!;

        return (
            <form className="form">
                <div className="form-group">
                    <label htmlFor="taskName">name</label>
                    <TaskNameInput value={t.name} onChange={this.onNameChange} />
                </div>

                <div className="form-group">
                    <label htmlFor="activeFrom">from</label>
                    <input
                        name="activeFrom"
                        type="date"
                        className="form-control"
                        value={DateFormat.formatIso(t.activeFrom)}
                        onChange={(e) => this.onDateChange('activeFrom', e.target.value)}
                    />
                    <label htmlFor="activeUntil">-</label>
                    <input
                        name="activeUntil"
                        type="date"
                        className="form-control"
                        value={DateFormat.formatIso(t.activeUntil)}
                        onChange={(e) => this.onDateChange('activeUntil', e.target.value)}
                    />
                    <small id="taskActiveHelp" className="form-text text-muted">
                        active duration (where task can we worked on).
                            </small>
                </div>

                <div className="row">
                    <button
                        type="button"
                        id="save"
                        className="btn btn-primary btn-md"
                        onClick={(e) => this.props.onTaskSave(t)}
                    >
                        {intl.get('task_save')}
                    </button>
                    &nbsp;
                    <button
                        type="button"
                        id="delete"
                        disabled={this.props.onTaskDelete ? false : true}
                        className="btn btn-md"
                        onClick={(e) => { if (this.props.onTaskDelete) {this.props.onTaskDelete(t); }}}
                    >
                        {intl.get('task_delete')}
                    </button>
                </div>
            </form>
        );
    }
}
