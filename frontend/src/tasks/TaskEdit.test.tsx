import * as React from 'react';

import { Task } from '../pd/task';
import * as moment from 'moment';
import { TaskEdit } from './TaskEdit';
import * as renderer from 'react-test-renderer';
import { locales } from '../i18n';
import * as intl from 'react-intl-universal';

describe('UI TaskEdit', () => {

    let task = new Task(1, 'test task with from', moment('2018-09-07').toDate(), null);

    it('display edit', () => {

        intl.init({ locales, currentLocale: 'en-US' });

        const fn1 = jest.fn();
        const fn2 = jest.fn();
        const fn3 = jest.fn();

        const json = renderer.create(
                <TaskEdit task={task} onTaskSave={fn1} onTaskEdited={fn2} onTaskDelete={fn3} />).toJSON();

        expect(json).toMatchSnapshot();

    });

});
