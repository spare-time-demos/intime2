import * as React from 'react';
import track from 'react-tracking';
import * as intl from 'react-intl-universal';
import { Task } from '../pd/task';
import Log from '../util/Logging';
import { TaskDisplay } from './TaskDisplay';
import { TaskEdit } from './TaskEdit';

export interface TasksProps {
    tasks: Task[];
    selectedTask: Task | undefined;

    onTasksLoad: () => void;
    onTaskSave: (task: Task) => void;
    onTaskNew: () => void;
    onTaskSelected: (task: Task) => void;
    onTaskDelete: (task: Task) => void;
}

/** task admin ui page */
@track({ page: 'Tasks' }, { dispatchOnMount: true })
export class TasksPage extends React.Component<TasksProps> {

    componentDidMount() {
        Log('TasksPage').info(`loading tasks on mount..`);
        this.props.onTasksLoad();
    }

    onTaskNew() {
        this.props.onTaskNew();
    }

    render() {
        return (
            <div>
                <br />
                {intl.get('tasks_page_title')}
                <br />
                <div className="row">
                    <br />
                    <button
                        type="button"
                        id="newTask"
                        className="btn btn-md"
                        onClick={
                            (e) => this.onTaskNew()
                        }
                    >
                        {intl.get('task_new')}
                    </button>
                    <br />
                </div>

                {this.props.tasks.map(
                    t => <TaskDisplay key={t.id} task={t} onTaskSelected={this.props.onTaskSelected} />)
                }

                {this.props.selectedTask != null
                    ? <TaskEdit
                        task={this.props.selectedTask}
                        onTaskSave={this.props.onTaskSave}
                        onTaskEdited={undefined}
                        onTaskDelete={this.props.selectedTask.id > 0 ? this.props.onTaskDelete : undefined}
                    />
                    : null
                }
            </div>
        );
    }
}
