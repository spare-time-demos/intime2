import * as React from 'react';

export interface TaskNameInputProps {
    value: string;
    onChange: (value: string) => void;
}

/** ui: on task line displayed. */
export class TaskNameInput extends React.Component<TaskNameInputProps> {

    onChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.onChange(e.target.value);
    }

    render() {
        return (
            <div>
                <input
                    name="taskName"
                    className="form-control"
                    value={this.props.value}
                    onChange={(e) => this.onChange(e)}
                />
                <small id="taskNameHelp" className="form-text text-muted">
                    naming the task ist required.
                </small>
            </div>
        );
    }
}
