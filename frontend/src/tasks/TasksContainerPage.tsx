import { connect, Dispatch } from 'react-redux';

import { StoreRoot } from '../actions/redux-reducers';
import { TasksPage } from './TasksPage';
import { loadAsyncTasks, TaskActions, selectTask, saveAsyncTask, deleteAsyncTask } from '../actions/task-actions';
import { Task } from '../pd/task';

export function mapStateToProps(store: StoreRoot) {
    return {
        tasks:          store.tasksState.tasks,
        selectedTask:   store.tasksState.selectedTask
    };
}

export function mapDispatchToProps(dispatch: Dispatch<TaskActions>) {
    return {
        onTasksLoad: () => {
            dispatch(loadAsyncTasks());
        },
        onTaskSave: (task: Task) => {
            dispatch(saveAsyncTask(task));
        },
        onTaskSelected: (task: Task) => {
            dispatch(selectTask(task));
        },
        onTaskNew: () => {
            dispatch(selectTask(new Task(-1, '')));
        },
        onTaskDelete: (task: Task) => {
            dispatch(deleteAsyncTask(task));
        },
    };
}

const TasksContainerPage = connect(mapStateToProps, mapDispatchToProps)(TasksPage);

/** tasks entry page. */
export default TasksContainerPage;
