import { connect, Dispatch } from 'react-redux';
import { StoreRoot } from './actions/redux-reducers';
import { AppStateActions } from './actions/app-state-actions';
import { MessageDisplay, MessageDisplayProps } from './MessageDisplay';

export function mapStateToProps(store: StoreRoot): MessageDisplayProps {
    return {
        message: store.appState.message,
        type: store.appState.messageType
    };
}

export function mapDispatchToProps(dispatch: Dispatch<AppStateActions>) {
    return {
    };
}

const MessageDisplayContainer = connect(mapStateToProps, mapDispatchToProps)(MessageDisplay);

export default MessageDisplayContainer;
