import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import store from './actions/store';

describe('react app', () => {

    /*nothing to see in this test. */
    it('renders without crashing', () => {
        const div = document.createElement('div');

        ReactDOM.render(
            <Provider store={store}>
                <App />
            </Provider>,
            div);
    });

    it('is in test mode', () => {
        const envMode = process.env.REACT_APP_ENVIRONMENT;
        expect(envMode).toEqual('test');
    });
});
