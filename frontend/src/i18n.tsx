import * as intl from 'react-intl-universal';
import { language } from './actions/app-state-actions';
import store from './actions/store';
import Log from './util/Logging';
/** localized text data in json files. */
export const locales = {
    'en-US': require('./locales/en-US.json'),
    'de-DE': require('./locales/de-DE.json'),
};

/** selection of supported languages (locales) */
export const SUPPORTED_LOCALES = [
    { name: 'English', value: 'en-US' },
    { name: 'Deutsch', value: 'de-DE' }
];

/** access i18n-intl universal */
export class I18NSupport {

    static init(currentLocale: string): Promise<void> {
        // init method will load "CLDR" locale data according to currentLocale
        // react-intl-universal is singleton, so you should init it only once in your app
        return intl.init({
            currentLocale: currentLocale,
            locales,
        })
            .then(() => {
                store.dispatch(language(currentLocale));
            });
    }

    /** detect best language (e.g. url parameter). */
    public static evaluateCurrentLocale(): string {

        let currentLocale = intl.determineLocale({
            urlLocaleKey: 'lang',
            cookieLocaleKey: 'lang'
        });

        if (!SUPPORTED_LOCALES.find(l => l.value === currentLocale)) {
            Log('i18n').info(`using fallback language en, unknown ${currentLocale}`);
            currentLocale = 'en-US';   // fallback/default language..
        } else {
            Log('i18n').info(`using language ${currentLocale}`);
        }

        return currentLocale;
    }
}
