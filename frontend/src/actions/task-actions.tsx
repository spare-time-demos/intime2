import { Task } from '../pd/task';
import { Dispatch } from 'react-redux';
import { TaskService } from '../service/task-service';
import { message, MessageType } from './app-state-actions';
import * as intl from 'react-intl-universal';

export class TasksState {
    tasks: Task[];
    selectedTask: Task | undefined;

    constructor() {
        this.tasks = [];
    }
}

export enum TaskActionTypes {
    LOAD_TASKS = 'LOAD_TASKS',
    SAVE_TASK = 'SAVE_TASK',
    SELECT_TASK = 'SELECT_TASK',
    OTHER_ACTION = 'OTHER_ACTION'
}

/** change task data. */
export interface SaveTaskAction {
    type: TaskActionTypes.SAVE_TASK;
    task: Task;
}

/** select task from list */
export interface SelectTaskAction {
    type: TaskActionTypes.SELECT_TASK;
    task?: Task;
}

/** load new tasks from store. */
export interface LoadTasksAction {
    type: TaskActionTypes.LOAD_TASKS;
    tasks: Task[];
}

export type TaskActions =
    | SaveTaskAction
    | LoadTasksAction
    | SelectTaskAction
    ;

export function saveTask(task: Task): SaveTaskAction {
    return {
        type: TaskActionTypes.SAVE_TASK,
        task
    };
}

/** save task */
export function saveAsyncTask(task: Task) {

    return function (dispatch: Dispatch<TasksState>) {
        const ts = new TaskService();
        ts.save(task).then(savedTask => {
            dispatch(saveTask(savedTask));
            if ( task.id < 0) {
                dispatch(loadAsyncTasks());
            }
            dispatch(message(intl.get('rest_task_saved'), MessageType.INFO));
        });
    };
}

/** save task */
export function deleteAsyncTask(task: Task) {

    return function (dispatch: Dispatch<TasksState>) {
        const ts = new TaskService();
        ts.delete(task)
        .then(deletedTaskId => {
            dispatch(selectTask(undefined));
            dispatch(loadAsyncTasks());
            dispatch(message(intl.get('rest_task_deleted'), MessageType.INFO));
        })
        .catch(err => {
            const text = `${err}`;
            dispatch(message(intl.get('rest_task_delete_error', { text: text }), MessageType.ERROR));
        });

    };
}

/** select task */
export function selectTask(task?: Task): SelectTaskAction {
    return {
        type: TaskActionTypes.SELECT_TASK,
        task
    };
}

/** save task */
export function loadTasksAction(tasks: Task[]): LoadTasksAction {
    return {
        type: TaskActionTypes.LOAD_TASKS,
        tasks: tasks
    };
}

/** load tasks data.. */
export function loadAsyncTasks() {

    return function (dispatch: Dispatch<TasksState>) {

        const ts = new TaskService();

        dispatch(message(intl.get('rest_tasks_loading'), MessageType.INFO));

        const tp = ts.getTasks()
            .then(tasks => {
                dispatch(message(intl.get('rest_tasks_loaded'), MessageType.INFO));
                dispatch(loadTasksAction(tasks));
            })
            .catch(err => {
                dispatch(message(intl.get('rest_tasks_load_error'), MessageType.ERROR));
            });

        return tp;
    };
}
