import { applyMiddleware, createStore, Store as ReduxStore } from 'redux';
import { StoreRoot } from './redux-reducers';
import reducers from './redux-reducers';
import thunk from 'redux-thunk';

/** create global redux store for intime app, thunk is used for async rest call actions. */
const store: ReduxStore<StoreRoot> = createStore(
    reducers,
    applyMiddleware(thunk)
);

export default store;
