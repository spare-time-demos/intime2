import * as moment from 'moment';

import store from './store';
import { changeHours, changeWeek } from './work-week-actions';
import { WorkWeek } from '../pd/work-week';
import { Task } from '../pd/task';

describe('redux store', () => {

    const date = moment('2018-02-03').toDate();
    const nextDate = moment('2018-02-04').toDate();

    beforeEach( () => {
        const initWorkWeek = [ 
                                WorkWeek.buildFromAnyDate(new Task(1, 'task 1'), date),
                                WorkWeek.buildFromAnyDate(new Task(2, 'task 2'), date) 
                             ]; 
        store.dispatch(changeWeek(initWorkWeek));
    });    

    it('store can be created', () => { 
        let st = store.getState();
        expect(st).toBeTruthy();
        expect(st.workWeeks.length).toBe(2);   // init beforeEach.
    });  
        
    it('store may change hours', () => {
        let st = store.getState();
        expect(st.workWeeks[0].getHour(date)).toBe(0);
        store.dispatch(changeHours(1, date, 3));
        st = store.getState();
        expect(WorkWeek.findByTask(st.workWeeks, 1).getHour(date)).toBe(3);
    });    
 
    it('store stores last change change of hours', () => {
        store.dispatch(changeHours(2, nextDate, 44));
        store.dispatch(changeHours(2, nextDate, 55));
        let st = store.getState();
        expect(WorkWeek.findByTask(st.workWeeks, 2).getHour(nextDate)).toBe(55);
    });    
     
});
