import { WorkWeek } from '../pd/work-week';
import { WorkItemService } from '../service/work-item-service';
import { StoreRoot } from './redux-reducers';
import { Dispatch } from 'react-redux';
import { TaskService } from '../service/task-service';
import { WorkWeekBuilder } from '../pd/work-week-builder';
import { message, MessageType } from './app-state-actions';
// tag::i18n[]
import * as intl from 'react-intl-universal';
// end::i18n[]
import Log from '../util/Logging';

export enum WeekActionTypes {
    CHANGE_WEEK  = 'CHANGE_WEEK',
    CHANGE_HOURS = 'CHANGE_HOURS',
    SAVE_WEEK    = 'SAVE_WEEK',
    OTHER_ACTION = 'OTHER_ACTION'
}

export enum DayActionTypes {
    CHANGE_DAY   = 'CHANGE_DAY',
    OTHER_ACTION = 'OTHER_DAY_ACTION'
}

export interface OtherAction {
    type: WeekActionTypes.OTHER_ACTION;
}

/** loaded new week tasks. */
export interface ChangeWeekAction {
    type: WeekActionTypes.CHANGE_WEEK;
    workWeek: WorkWeek[];
}

export interface SaveWeekAction {
    type: WeekActionTypes.SAVE_WEEK;
    workWeek: WorkWeek[];
}

/** navigate +/-. */
export interface ChangeDayAction {
    type: DayActionTypes.CHANGE_DAY;
    date: Date;
}

/** working hours edited on ui. */
export interface ChangeHourAction {
    type: WeekActionTypes.CHANGE_HOURS;
    taskId: number;
    day: Date;
    changedHours: number;
}

export type WeekActions =
        | ChangeHourAction
        | ChangeDayAction
        | ChangeWeekAction
        | SaveWeekAction
        | OtherAction;

/** new hours edited on ui. */
export function changeHours(taskId: number, day: Date, changedHours: number): ChangeHourAction {
    return  {
        type: WeekActionTypes.CHANGE_HOURS,
        taskId,
        day,
        changedHours
    };
}

/** load given week data from rest services.. */
export function loadAsyncWeek(date: Date)  {

    return function(dispatch: Dispatch<StoreRoot>) {

        const wis = new WorkItemService();
        const ts = new TaskService();

        // tag::i18n[]
        // ...
        dispatch(message(intl.get('rest_week_loading', { day: date}), MessageType.INFO)); // <1>
        // end::i18n[]

        const tp = ts.getTasksForWeek(date);

        wis.getWorkWeek(date).then(wi => {
            tp.then(tasks => {
                let ww = WorkWeekBuilder.buildWeekFromTasksAndItems(date, tasks, wi);
                dispatch(changeDay(date));
                dispatch(changeWeek(ww));
                dispatch(message(intl.get('rest_week_loaded', { day: date}), MessageType.INFO));
            });
        })
        .catch(err => {
            const text = `${err}`;
            dispatch(message(intl.get('rest_week_load_error', { day: date, text: text }), MessageType.ERROR));
        });
    };
}

export function changeDay(date: Date): ChangeDayAction {
    return  {
        type: DayActionTypes.CHANGE_DAY,
        date
    };
}

export function changeWeek(workWeek: WorkWeek[]): ChangeWeekAction {
    Log('action').debug(`change to new week ${workWeek.toString()}..`);

    return  {
        type: WeekActionTypes.CHANGE_WEEK,
        workWeek: workWeek
    };
}

/* save complete weeks work data. */
export function saveWeek(workWeek: WorkWeek[]) {

    Log('action').debug('put loaded data into store week here..');

    return function(dispatch: Dispatch<StoreRoot>) {

        const wis = new WorkItemService();

        const date = workWeek[0].startDate;

        dispatch(message(intl.get('rest_week_saving', { day: date}), MessageType.INFO));

        const workItems =  workWeek.map(ww => ww.toItems())
                                   .filter(wi => wi != null)
                                   .reduce((a, b) => a.concat(b) );

        wis.save(workItems).then(hoursCount => {
            dispatch(message(intl.get('rest_week_saved', { day: date, hours: hoursCount}), MessageType.INFO));

            return  {
                type: WeekActionTypes.SAVE_WEEK,
                workWeek: workWeek
            };
        })
        .catch(err => {
            dispatch(message(intl.get('rest_week_save_error', { day: date, text: err}), MessageType.ERROR));
        });
    };

}
