import { combineReducers } from 'redux';

import { WeekActions, WeekActionTypes, ChangeDayAction, DayActionTypes } from './work-week-actions';
import { AppStateActions, AppStateActionTypes, AppState, MessageType } from './app-state-actions';
import { TaskActions, TaskActionTypes, TasksState } from './task-actions';

import { WorkWeek } from '../pd/work-week';
import Log from '../util/Logging';

/** app error messages and language changes .. */
export function appStateReducer(appState: AppState = new AppState(), action: AppStateActions): AppState {
    switch (action.type) {
        case AppStateActionTypes.MESSAGE: {
            action.appState.language = appState.language;
            return action.appState;
        }
        case AppStateActionTypes.LANGUAGE: {
            const lng = action.language;
            Log('reducer').debug(`reducer: changing language to ${lng}`);
            return {
                message: `changed language to ${lng}.`,
                messageType: MessageType.INFO,
                language: lng
            };
        }
        default:
            return appState;
    }
}

/** change edited week or day. */
export function changeDayReducer(state: Date = new Date(), action: ChangeDayAction): Date {

    switch (action.type) {
        case DayActionTypes.CHANGE_DAY: {
            return action.date;
        }
        default:
            return state;
    }
}

/** change on a given work weeks task data. */
export function changeWorkWeeksReducer(state: WorkWeek[] = [], action: WeekActions): WorkWeek[] {

    let ww = state.slice();

    switch (action.type) {
        case WeekActionTypes.CHANGE_HOURS: {
            let w = WorkWeek.findByTask(ww, action.taskId);
            if (w && w.isInWeek(action.day)) {
                w.setHour(action.day, action.changedHours);
            }
            return ww;
        }
        case WeekActionTypes.CHANGE_WEEK: {
            return action.workWeek;
        }
        case WeekActionTypes.SAVE_WEEK: {
            return action.workWeek;
        }
        default:
            return ww;
    }
}

/** change task data. */
export function changeTasksReducer(
    state: TasksState = new TasksState(),
    action: TaskActions)
    : TasksState {

    switch (action.type) {
        case TaskActionTypes.SAVE_TASK: {
            return {...state, selectedTask: undefined};
        }
        case TaskActionTypes.LOAD_TASKS: {
            return {...state, tasks: action.tasks, selectedTask: undefined};
        }
        case TaskActionTypes.SELECT_TASK: {
            return {...state, selectedTask: action.task};
        }
        default:
            return state;
    }
}

/** the complete redux store of this app. */
export type StoreRoot = {
    workWeeks:  WorkWeek[]
    date:       Date
    tasksState: TasksState
    appState:   AppState
};

const reducers = combineReducers<StoreRoot>({
    workWeeks: changeWorkWeeksReducer,
    date: changeDayReducer,
    tasksState: changeTasksReducer,
    appState: appStateReducer});

export default reducers;
