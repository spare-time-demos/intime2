import Log from '../util/Logging';
export enum AppStateActionTypes {
    MESSAGE  = 'MESSAGE',
    LANGUAGE = 'LANGUAGE'
}

export enum MessageType {
    INFO    = 'i',
    WARNING = 'w',
    ERROR   = 'e',
    SUCCESS = 's'
}

export class AppState {
    message: string;
    messageType: MessageType;
    language: string;
}

/** display message of app state (errors, hints etc.). */
export interface MessageAction {
    type: AppStateActionTypes.MESSAGE;
    appState: AppState;
}

export interface LanguageAction {
    type: AppStateActionTypes.LANGUAGE;
    language: string;
}

export type AppStateActions =
        | MessageAction
        | LanguageAction
        ;

/** display new message. */
export function message(msg: string, type: MessageType = MessageType.SUCCESS): MessageAction {

    Log('action').debug(`changing message ${msg}`);

    const newState = new AppState();
    newState.message = msg;
    newState.messageType = type;
    return  {
        type: AppStateActionTypes.MESSAGE,
        appState: newState
    };
}

/** change i18n language. */
export function language(lng: string): LanguageAction {
    Log('action').debug(`action: changing language ${lng}`);

    return  {
        type: AppStateActionTypes.LANGUAGE,
        language: lng
    };
}
