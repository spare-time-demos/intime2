import * as React from 'react';
import track from 'react-tracking';
import * as intl from 'react-intl-universal';

/** projects admin ui page */
@track({ page: 'Projects' }, { dispatchOnMount: true })
export class ProjectsPage extends React.Component {

    render() {
      return (
        <div>
            <br/>
            {intl.get('projects_page_title')}
        </div>
      );
    }
}
