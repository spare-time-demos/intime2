import * as React from 'react';
import track from 'react-tracking';
import * as intl from 'react-intl-universal';

import { BarChart, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Cell, Bar } from 'recharts';
import { WorkWeek } from '../pd/work-week';

interface ReportingProps {
    workWeeks: WorkWeek[];
}

/** reporting ui page. */
@track({ page: 'Reporting' }, { dispatchOnMount: true })
export class ReportingPage extends React.Component<ReportingProps> {

    weekDisplay(): string {
        return this.props.workWeeks == null || this.props.workWeeks.length === 0
                ? 'no week selected'
                : intl.get('date', {day: this.props.workWeeks[0].getStartDate()} );
    }

    map2PieData(ww: WorkWeek[]): Object[] {
        return ww.map( w => ({ id: w.id, name: w.task.name, value: w.sum() }));
    }

    /** { name: 'Monday', uv: 3, pv: 5, amt: 1 }, */
    map2BarData(ww: WorkWeek[]): Object[] {

        if (ww.length === 0) {
            return [];
        }

        const d = [0, 1, 2, 3, 4, 5, 6]
                .map( i => {

                    let sumHours = ww.map(w => w.hours[i])
                                     .reduce((sum, current) => sum + current);

                    let day = intl.get('date', {day: ww[0].getDateOf(i)});

                    let obj = ({
                       name: day,
                       sum: sumHours,
                    });

                    ww.forEach( w => {
                        // tslint:disable-next-line:no-string-literal
                        obj[w.task.id] = w.hours[i];
                    });
                    return obj;
                });

        return d;
    }

    // tag::rechart[]

    render() {
        const colors = ['#8884d8', '#82ca9d', '#82c000', '#FF8042'];

        return (
            <div>
                <br />
                {intl.get('reporting_page_title')} {this.weekDisplay()}

                <BarChart
                    width={800}
                    height={200}
                    data={this.map2BarData(this.props.workWeeks)}
                    margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis label="h" />
                    <Tooltip />
                    <Legend />

                    {
                        this.props.workWeeks.map((w, index) => (
                            <Bar
                                key={w.task.id}
                                dataKey={w.task.id}
                                name={w.task.name}
                                stackId="task"
                                fill={colors[index % colors.length]}
                            />
                        ))
                    }

                </BarChart>

                <PieChart width={800} height={300}>
                    <Tooltip />
                    <Legend  />
                    <Pie
                            startAngle={0}
                            endAngle={360}
                            data={this.map2PieData(this.props.workWeeks)}
                            dataKey="value"
                            nameKey="name"
                            innerRadius={20}
                            outerRadius={80}
                            label={true}
                            fill="#8884d8"
                    >
                    {
                     this.map2PieData(this.props.workWeeks).map((entry, index) => (
                        <Cell key={index} fill={colors[index % colors.length]}/>
                        ))
                    }
                    </Pie>
                </PieChart>

            </div>
        );
    }

    // end::rechart[]
}
