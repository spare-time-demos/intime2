import { connect, Dispatch } from 'react-redux';
import { WeekActions } from '../actions/work-week-actions';

import { StoreRoot } from '../actions/redux-reducers';
import { ReportingPage } from './ReportingPage';

export function mapStateToProps(store: StoreRoot) {
    return {
        workWeeks: store.workWeeks,
    };
}

export function mapDispatchToProps(dispatch: Dispatch<WeekActions>) {
    return {
    };
}

const ReportingContainerPage = connect(mapStateToProps, mapDispatchToProps)(ReportingPage);

/** reporting entry page. */
export default ReportingContainerPage;
