import { locales } from './i18n';
import * as intl from 'react-intl-universal';

describe('test i18n', () => {

    it('get de-DE text', () => {
        intl.init({ locales, currentLocale: 'de-DE' });
        expect(intl.get('APP_TITLE')).toBe('Willkommen zu inTime2');
    });

    it('get en-US text', () => {
        intl.init({ locales, currentLocale: 'en-US' });
        expect(intl.get('APP_TITLE')).toBe('Welcome to inTime2');
    });

    it('get us date', () => {
        intl.init({ locales, currentLocale: 'en-US' });
        expect(intl.get('time_entry_selected_day', {day: new Date(2018, 9, 11)})).toBe('Oct 11, 2018');
    });

    // skip and investigate, will not run on ci environment?
    it.skip('get german date (datum)', () => {
        intl.init({ locales, currentLocale: 'de-DE' });
        expect(intl.get('date', {day: new Date(2018, 10, 12)})).toBe('2018-11-12');
    });

});
