import { WorkItemService } from './work-item-service';

import * as fetchMock from 'fetch-mock';
import { WorkItem } from '../pd/work-item';
import { Task } from '../pd/task';
import { UrlUtil } from './url-util';

describe('WorkItemService REST', () => {
    afterEach(fetchMock.restore);

    it('REST mocked read call should work..', async () => {
        const dateString = '2018-03-06';
        const newLocal = new UrlUtil().buildRestUrl(`work/week/${dateString}`);

        fetchMock.get(
            newLocal,
            {
                status: 200,
                body:
                  [
                    {
                        'id': 2,
                        'date': '2018-03-07',
                        'hours': 5,
                        'task': {
                            'id': 1,
                            'name': 'mocked-dummy'
                        }
                    },
                    {
                        'id': 3,
                        'date': '2018-03-08',
                        'hours': 2,
                        'task': {
                            'id': 1,
                            'name': 'dummy'
                        }
                    }
                  ]
                ,
            }
        );

        const srv = new WorkItemService();
        const workWeeks = await srv.getWorkWeek(new Date(dateString));

        expect(workWeeks).toBeTruthy();
        expect(workWeeks.length).toBe(2);
        expect(workWeeks[0].hours).toBe(5);
        expect(workWeeks[0].task.id).toBe(1);
        expect(workWeeks[0].task.name).toMatch(/mocked-dummy/);
        expect(workWeeks[0].date).toEqual(new Date('2018-03-07'));
        expect(workWeeks[1].date).toEqual(new Date('2018-03-08'));
    });

    it('REST mocked save call should work..', async () => {

        const newLocal = new UrlUtil().buildRestUrl(`work`);

        let w1 = new WorkItem(new Task(1, 'xx'), new Date(), 2);
        let w2 = new WorkItem(new Task(2, 'xy'), new Date(), 2);

        fetchMock.post(
            newLocal,
            {
                status: 200,
                body: '2',
            }
        );

        const srv = new WorkItemService();
        const saveCount = await srv.save([w1, w2]);
        expect(saveCount).toBe(2);
    });

});
