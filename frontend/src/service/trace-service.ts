import { UrlUtil } from './url-util';
import Log from '../util/Logging';

/** http rest access: /trace/track. */
export class TraceService {

    public sendTrack(data: Object): Promise<void> {

        const url = new UrlUtil().buildRestUrl(`trace/track`);
        const json = `[${JSON.stringify(data)}]` ;

        Log('trace').debug(`about to send tracking to ${url}: ${json}`);

        return fetch(url, {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: json
            })
            .then((res: Response) => {
                    if (res.status >= 400) {
                        Log('trace').error(`Bad response from tracking server: ${res.status} ${res.statusText}.`);
                    }
                    Log('trace').debug(`tracking data sent.`);
                    return;
                })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('trace').fatalException(`error posting tracking data ${url}`, err);
            });
    }

}
