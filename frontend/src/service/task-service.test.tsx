import { TaskService } from './task-service';

import * as fetchMock from 'fetch-mock';
import { UrlUtil } from './url-util';
import Log from '../util/Logging';
import * as moment from 'moment';
import { Task } from '../pd/task';

describe('TaskService REST', () => {
    afterEach(fetchMock.restore);

    it('REST mocked call should work..', async () => {
        const dateString = '2018-03-06';
        const newLocal = new UrlUtil().buildRestUrl(`task/week/${dateString}`);

        fetchMock.get(
            newLocal,
            {
                status: 200,
                body:
                    [
                        { 'id': 1, 'name': 'dummy', 'activeFrom': '2018-01-01' },
                        { 'id': 5, 'name': 'dummy task 2' }
                    ],
            }
        );

        const srv = new TaskService();
        const tasks = await srv.getTasksForWeek(new Date(dateString));
        expect(tasks).toBeTruthy();
        expect(tasks.length).toBe(2);
        expect(tasks[0].id).toBe(1);
        expect(tasks[1].id).toBe(5);
        expect(tasks[0].name).toMatch(/dummy/);
        expect(tasks[1].name).toMatch(/dummy task 2/);
        expect(tasks[0].isActive(moment('2018-02-03').toDate())).toBeTruthy();
        expect(tasks[0].isActive(moment('2016-02-03').toDate())).toBeFalsy();
    });

    it('REST wrong call, error handling should work..', async () => {
        const dateString = '2018-03-06';
        const newLocal = new UrlUtil().buildRestUrl(`task/week/${dateString}`);

        fetchMock.mock(newLocal, 500);

        const srv = new TaskService();
        try {
            const promisedTasks = srv.getTasksForWeek(new Date(dateString));
            await promisedTasks
                .then(response => {
                    fail(`did not expect success!! ${response}`);
                })
                .catch(err => {
                    Log().info(`expected error found ${err}`);
                }
                );
            // fail('should jump to catch');
        } catch (err) {
            fail(`did not exception, but failed promise! ${err}`);
        }
    });

    it('REST mocked save call should work..', async () => {

        const newLocal = new UrlUtil().buildRestUrl(`saveTask`);

        let newTask = new Task(-1, 'task new');

        fetchMock.post(
            newLocal,
            {
                status: 200,
                body: { 'id': 37, 'name': 'task new saved' }
            }
        );

        const srv = new TaskService();
        const savedTask = await srv.save(newTask);
        expect(savedTask.name).toBe('task new saved');
        expect(savedTask.id).toBe(37);
    });

    it('REST mocked save call should work even with dates..', async () => {

        const newLocal = new UrlUtil().buildRestUrl(`saveTask`);

        let newTask = new Task(-1, 'task new', new Date(2016, 3, 2), new Date(2018, 11, 20) );

        fetchMock.post(
            newLocal,
            {
                status: 200,
                body: { 'id': 37, 'name': 'task new saved' }
            }
        );

        const srv = new TaskService();
        const savedTask = await srv.save(newTask);

        expect(savedTask.name).toBe('task new saved');
        expect(savedTask.id).toBe(37);

        const lastCall = fetchMock.lastCall();
        // tslint:disable-next-line:no-string-literal
        const postParam = (lastCall[1] as fetchMock.MockRequest)['body'];

        expect(postParam)
            .toContain('{"id":-1,"name":"task new","activeFrom":"2016-04-02","activeUntil":"2018-12-20"}');
    });

    it('REST mocked delete call should work..', async () => {

        const newLocal = new UrlUtil().buildRestUrl(`deleteTask`);
        const taskToDelete = new Task(5, 'dummy delete task');

        fetchMock.post(
            newLocal,
            {
                status: 200,
                body: '5'
            }
        );

        const srv = new TaskService();
        const deletedTaskId = await srv.delete(taskToDelete);
        expect(deletedTaskId).toBe(5);
    });

});
