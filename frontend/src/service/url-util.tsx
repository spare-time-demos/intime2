/** building URLs. */
export class UrlUtil {

    /**
     * build the HTTP URL to REST Backend.
     *
     * local will be http://localhost:8080/localPath,
     * on deployment it will be /
     */
    public buildRestUrl(localPath: string): string {

        const baseUrl = process.env.REACT_APP_REST_SERVICE_BASE_URL;
        const url = `${baseUrl}${localPath}`;
        return url;
    }
}
