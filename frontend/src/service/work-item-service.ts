import { WorkItem } from '../pd/work-item';

import * as moment from 'moment';
import { UrlUtil } from './url-util';
import * as intl from 'react-intl-universal';
import Log from '../util/Logging';

/** http rest access: work/week/2018-03-06 */
export class WorkItemService {

    public getWorkWeek(date: Date): Promise<WorkItem[]> {

        let param = moment(date).format('YYYY-MM-DD');
        let url = new UrlUtil().buildRestUrl(`work/week/${param}`);

        Log('WorkItemService').info(`about to call ${url}`);

        return fetch(url)
            .then((res: Response) => {
                if (res.status >= 400) {
                    throw new Error(intl.get('rest_call_http_error', { code: res.status, text: res.statusText }));
                }

                return this.parseJSON(res);
            })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('WorkItemService').fatalException(`error call ${url}`, err);
                throw err;
            });
    }

    /** save work items. */
    public save(workItems: WorkItem[]): Promise<number> {

        let url = new UrlUtil().buildRestUrl(`work`);

        let jsonPayload: string = JSON.stringify(workItems);

        Log('WorkItemService').debug(`about to save items with post to ${url} for ${jsonPayload}`);

        return fetch(url, {
                method: 'POST',
                headers: {
                    'user-agent': 'intime2-frontend',
                    'content-type': 'application/json'
                  },
                body: jsonPayload
            })
            .then((res: Response) => {
                if (res.status >= 400) {
                    throw new Error(
                        intl.get('rest_call_http_error',
                                 { code: res.status.toString(), text: res.statusText })
                        );
                }
                return res.json().then(js => JSON.parse(js) );
            })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('WorkItemService').fatalException(`error save ${url}`, err);
                throw err;
            });
    }

    private parseJSON(response: Response): Promise<WorkItem[]> {
        return response.json().then(js => js.map((data: WorkItem) => WorkItem.fromJson(data)));
    }
}
