import { InTime2swaggerRESTAPI } from './inTime2swaggerRESTAPI';
import { Task } from './models/index';

import { Task as TTask } from '../../pd/task';

describe('demo call autorest generated client', () => {

    /** skipping test, needs real rest server running. */
    it.skip('reading tasks', async () => {

        const client = new InTime2swaggerRESTAPI('http://localhost:8080/');
        const tasks = await client.taskMethod();
        expect(tasks.length).not.toBe(0);
        expect(tasks[0].name).toBeTruthy();
        expect(tasks[0].activeFrom).toBeTruthy();

    });

    /** skipping test, needs real rest server running. */
    it.skip('saving a task', async () => {
        const client = new InTime2swaggerRESTAPI('http://localhost:8080/');

        // const task = new Task(-1, 'autorest saved task', new Date());
        const task: Task = {
            id: -1,
            name: 'a autorest created task.',
            activeFrom: new Date()
        };

        const savedTask = await client.saveTask(task);
        expect(savedTask.id).toBeTruthy();
        expect(savedTask.id).toBeGreaterThan(1);
        expect(savedTask.name).toBe('a autorest created task.');
        expect(savedTask.activeUntil).toBeFalsy();
        expect(savedTask.activeFrom).toBeTruthy();

    });

});
