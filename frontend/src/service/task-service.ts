import { Task } from '../pd/task';

import * as moment from 'moment';
import { UrlUtil } from './url-util';
import Log from '../util/Logging';
import * as intl from 'react-intl-universal';

/** http rest access: /task/week/2018-03-02 */
export class TaskService {

    public getTasks(): Promise<Task[]> {

        const url = new UrlUtil().buildRestUrl(`task`);

        Log('TaskService').info(`about to call tasks ${url}`);

        return fetch(url)
            .then((res: Response) => {
                if (res.status >= 400) {
                    throw new Error(`Bad response from server: ${res.status} ${res.statusText}.`);
                }
                return this.parseJSON(res);
            })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('TaskService').fatalException(`error get ${url}`, err);
                throw err;
            });
    }

    public getTasksForWeek(date: Date): Promise<Task[]> {

        let param = moment(date).format('YYYY-MM-DD');
        const url = new UrlUtil().buildRestUrl(`task/week/${param}`);

        Log('TaskService').info(`about to call tasks ${url}`);

        return fetch(url)
            .then((res: Response) => {
                if (res.status >= 400) {
                    throw new Error(`Bad response from server: ${res.status} ${res.statusText}.`);
                }
                return this.parseJSON(res);
            })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('TaskService').fatalException(`error get ${url}`, err);
                throw err;
            });
    }

    /** save task. */
    public save(task: Task): Promise<Task> {

        let url = new UrlUtil().buildRestUrl(`saveTask`);

        let jsonPayload: string = JSON.stringify(task);

        Log('TaskService').debug(`about to save task to ${url} for ${jsonPayload}`);

        return fetch(url, {
            method: 'POST',
            headers: {
                'user-agent': 'intime2-frontend',
                'content-type': 'application/json'
            },
            body: jsonPayload
        })
            .then((res: Response) => {
                if (res.status >= 400) {
                    throw new Error(
                        intl.get('rest_call_http_error',
                                 { code: res.status.toString(), text: res.statusText })
                    );
                }
                return res.json().then(js => Task.fromJson(js));
            })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('TaskService').fatalException(`error save ${url}`, err);
                throw err;
            });
    }

    /** delete task. */
    public delete(task: Task): Promise<Number> {

        let url = new UrlUtil().buildRestUrl(`deleteTask`);

        let jsonPayload: string = `${task.id}`;

        Log('TaskService').debug(`about to delete task ${url} for ${jsonPayload}`);

        return fetch(url, {
            method: 'POST',
            headers: {
                'user-agent': 'intime2-frontend',
                'content-type': 'application/json'
            },
            body: jsonPayload
        })
            .then((res: Response) => {
                if (res.status > 400) {
                    throw new Error(
                        intl.get('rest_call_http_error',
                                 { code: res.status.toString(), text: res.statusText })
                    );
                }
                if (res.status === 400) {
                    throw new Error(
                        intl.get('rest_call_http_error',
                                 { code: res.status.toString(), text: `not allowed tot delete task..` })
                    );
                }
                return res.text();
            })
            .then((txt) => {
                return 5;
            })
            // tslint:disable-next-line:no-any
            .catch((err: any) => {
                Log('TaskService').fatalException(`error delete task ${url}`, err);
                throw err;
            });
    }

    private parseJSON(response: Response): Promise<Task[]> {
        return response.json().then(js => js.map((data: Task) => Task.fromJson(data)));
    }
}
