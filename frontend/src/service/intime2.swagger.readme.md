# inTime2 autorest configuration readme.md

> see https://aka.ms/autorest

## Input swagger api descriptions

``` yaml
input-file: intime2.swagger.json
```

## Typescript client generation

``` yaml
typescript:
  - output-folder: api
```

