import * as React from 'react';
import { MessageType } from './actions/app-state-actions';
import * as classnames from 'classnames';
import track from 'react-tracking';

export interface MessageDisplayProps {
    message: string;
    type: MessageType;
}

/** render message or hide. */
@track((props: MessageDisplayProps) => {
        return {
            component: 'MessageDisplay',
            message: props.message
        };
})
export class MessageDisplay extends React.Component<MessageDisplayProps> {

    constructor(props: MessageDisplayProps) {
        super(props);
    }

    render() {

        let classes = this.props.type === MessageType.ERROR
            ? classnames('col-12 alert alert-danger')
            : (this.props.type === MessageType.WARNING
                ? classnames('col-12 alert alert-warning')
                : classnames('col-12 alert alert-success'));

        if (this.props.message != null && this.props.message.length > 0) {
            return (
                <div className="row">
                    <div className={classes} data-test="messageDisplay">
                        {this.props.message}
                    </div>
                </div>
            );
        } else {
            return (null);
        }
    }
}
