import { JL } from 'jsnlog';
import { UrlUtil } from '../service/url-util';

/** logging wrapper for jsnlog. not sure if to keep it yet.
 *  see http://js.jsnlog.com/.
 * @author man@home
 */
export class Logger {

    private name?: string;

    constructor(name?: string) {
        this.name = name == null ? 'na' : name;
    }

    public debug(msg: string) {
        JL(this.name).debug(msg);
    }

    public info(msg: string) {
        JL(this.name).info(msg);
    }

    public error(msg: string) {
        JL(this.name).error(msg);
    }

    public fatalException(msg: string, exception: Object) {
        JL(this.name).fatalException(msg, exception);
    }

}

/** logger factory. */
export default function Log(name?: string): Logger {
    return new Logger(name);
}

/** Create beforeSend handler, add http header on remote logging calls */
function beforeSendHandler(xhr: XMLHttpRequest) {
    xhr.setRequestHeader('X-intime2-log', 'v1');
}

/** one time initialization code for logging (jsnlog). */
export function LogInit() {

    const logUrl = new UrlUtil().buildRestUrl(`trace/log`);

    // remote appender: send logs back to rest server
    const remoteAppender = JL.createAjaxAppender('remote_appender');
    remoteAppender.setOptions({
        'url':          logUrl,             // api endpoint log entries will get forwarded to
        'level':        JL.getInfoLevel(),  // remote logging only info level and above
        'batchSize':    10,                 // wait and batch 10 log events before sending
        'maxBatchSize': 20,                 // store no more than 20 log events
        'batchTimeout': 1000,               // wait for batch only max 10000 msec before send
        'beforeSend':   beforeSendHandler   // add custom logic before send.
    });

    // local appender: display in browser console
    const consoleAppender = JL.createConsoleAppender('consoleAppender');

    JL.setOptions({'requestId': 'TODO:clientId'}); // TODO: mrj add clientId or sessionId here

    JL().setOptions({
        'appenders': [consoleAppender, remoteAppender],
    });

    Log('LogInit').info('initialized logging with JsnLog');
}

/** Code included in jsnlog.js to set a handler that logs uncaught JavaScript errors to
 * the server side log.
 */
if (window && !window.onerror) {
    // tslint:disable-next-line:typedef
    window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
        JL('onerrorLogger').fatalException({
            'msg': 'Uncaught Exception',
            'errorMsg': errorMsg, 'url': url,
            'line number': lineNumber, 'column': column
        },
                                           errorObj
        );

        return false;
    };
}
