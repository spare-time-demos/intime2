import * as  Raven from 'raven-js';

describe('simple logging calls (ensure to exceptions break calling code)', () => {

    it('send error information to sentry.io', () => {
        try {
            Raven.config('https://818455fb953b4591a4b97c992705e6bb@sentry.io/1205622')
                 .install();

            throw Error('unit test error');
        } catch (error) {
            Raven.captureException(error);
        }
    });

    it('send error information to sentry.io', () => {
            Raven.config('https://818455fb953b4591a4b97c992705e6bb@sentry.io/1205622')
                 .install();

            const r = Raven.captureMessage('hello message from unit test sentry.test.tsx');
            const eventId = r.lastEventId();

            expect(eventId).toBeTruthy();
    });
});
