import Log from './Logging';
import { LogInit } from './Logging';

describe('simple logging calls (ensure to exceptions break calling code)', () => {

    it('logging without exceptions', () => {
        Log('test').debug('debug log line');
        Log().info('info log line');
    });

    it('logging exceptions', () => {
        try {
            throw new Error('intended exception');
        } catch ( ex ) {
            Log('HANDLE').fatalException('error', ex);
        }
    });

    it('logging without exceptions after custom init', () => {
        LogInit();
        Log('test').debug('debug log line after init');
        Log().info('info log line after init');
    });
});
