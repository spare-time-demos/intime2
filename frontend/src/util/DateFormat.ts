import * as moment from 'moment';

export class DateFormat {

    /** iso date (no time) format (YYYY-MM-DD) */
    public static formatIso(date?: Date | null): string {
        if (!date) {
            return '';
        }

        return moment(date).format('YYYY-MM-DD');
    }

    /** default format (simplistic german at the moment.) */
    public static format(date: Date): string {
        if (date === null) {
            return '';
        }

        return this.formatMoment(moment(date));
    }

    /** default format (simplistic german at the moment.) */
    public static formatMoment(date: moment.Moment): string {
            if (date === null) {
                return '';
            }
            return date.format('DD.MM.YYYY');
    }

    /** parse date from YYYY-MM-DD format string. */
    // tslint:disable-next-line:no-empty
    public static parseIsoFormat(dateString: string): Date | null {
        if ( !dateString ) {return null; }

        const m = moment(dateString, moment.ISO_8601);
        return m.isValid ? m.toDate() : null;
    }

    /** calc first day of week. */
    public static startOfWeek(anyDayInWeek: Date): Date {
        return moment(anyDayInWeek).clone().startOf('isoWeek').toDate();
    }

}
