= inTime2 readme
man@home

image:https://spare-time-demos.gitlab.io/intime2/images/intime2_logo_small.svg[title="inTime2 logo"]

build pipeline results:
image:https://gitlab.com/spare-time-demos/intime2/badges/master/pipeline.svg[link="https://gitlab.com/spare-time-demos/intime2/commits/master",title="pipeline status"] 
image:https://codecov.io/gl/spare-time-demos/intime2/branch/master/graph/badge.svg[link=https://codecov.io/gl/spare-time-demos/intime2,title="codecov.io"]
image:https://codebeat.co/badges/961d8dca-d204-4c5f-b31f-048e053bb2d4[link="https://codebeat.co/projects/gitlab-com-spare-time-demos-intime2-master"]
image:https://sonarcloud.io/api/project_badges/measure?project=intime2-frontend&metric=sqale_rating[title="inTime2 frontend rating"]


fallback builds:
image:https://api.shippable.com/projects/5aecc0aad42d150700abb4e2/badge?branch=master[link="https://app.shippable.com/gitlab.com/spare-time-demos/intime2",title="shippable backend test build"]
image:https://app.codeship.com/projects/bf566cf0-320f-0136-a82e-125c26f8fcf4/status?branch=master[link="https://app.codeship.com/projects/288880",title="codeship backend test build"]


== what it is

a demo project only, mimicking a project hour log software (time tracker)
used to experiment with project layouts and structure.

NOTE: milestone reached (environment setup completed), development stalled..

== documentation

see gitlab pages hosted on https://spare-time-demos.gitlab.io/intime2
with current build and test results, technical documentation and so on..

== shortcut to get running

get running in development mode

.run the rest kotlin server backend:
[source, sh]
----
./gradlew backend:bootRun
----

.run the react typescript spa frontend:
[source, sh]
----
cd frontend
npm start
----

== submodules in this repo

- https://gitlab.com/spare-time-demos/intime2/tree/master/frontend[/frontend]     react frontend
- https://gitlab.com/spare-time-demos/intime2/tree/master/backend[/backend]       kotlin/spring boot rest backend and database scripts
- https://gitlab.com/spare-time-demos/intime2/tree/master/docs[/docs]               manual documentation and documentation builds
- https://gitlab.com/spare-time-demos/intime2/tree/master/qa/e2e[/qa/e2e]         e2e test scripts
- https://gitlab.com/spare-time-demos/intime2/tree/master/deployment[/deployment] docker build and openshift deployment
