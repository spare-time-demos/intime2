describe('happy path inTime2 on staging', function() {

    let url      = 'http://intime2-intime2.7e14.starter-us-west-2.openshiftapps.com/#/entry/2018-03-12'
    let urlTasks = 'http://intime2-intime2.7e14.starter-us-west-2.openshiftapps.com/#/tasks'

    it('navigate to tasks page', function() {

        cy.visit(url)
        cy.contains('inTime2')
        cy.contains('Save')

        cy.contains('Tasks').click()
        cy.contains('Tasks loaded.')      
    })

    it('create new task', function() {

        cy.visit(urlTasks)
        cy.contains('Tasks loaded.')      

        cy.get('#newTask').click()
        cy.contains('name')      
        cy.contains('active duration')      
    })

    it('edit first task', function() {

        cy.visit(urlTasks)
        cy.contains('Tasks loaded.')      

        cy.contains('edit').click()
        cy.contains('name')      
        cy.contains('active duration')      
    })

  })