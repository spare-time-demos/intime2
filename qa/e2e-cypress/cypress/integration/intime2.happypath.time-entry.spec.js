describe('happy path inTime2 on staging', function() {

    let url = 'http://intime2-intime2.7e14.starter-us-west-2.openshiftapps.com/#/entry/2018-03-12'

    it('visit staging deployment pages on openshift', function() {

        cy.visit(url)
        cy.contains('inTime2')
        cy.contains('Save')

        cy.contains('Reporting').click()
        cy.contains('Reports')      

        cy.contains('Projects').click()
        cy.contains('not yet implemented')      
    })

    it('change language via dropdown', function() {

        cy.visit(url)
        cy.contains('inTime2')

        cy.get('select').select('Deutsch')
        cy.contains('Speichern')      
        cy.contains('Willkommen zu')      

        cy.get('select').select('English')
        cy.contains('Save')      
        cy.contains('Welcome to')      
    })

    it('edit and save', function() {

        cy.visit(url)
        cy.contains('inTime2')

        cy.get('input[data-test="hourInput"]').first().clear().type('8')
        cy.get('input[data-test="hourInput"]').eq(1).clear().type('3')

        cy.screenshot('intime2.timeEntry.example')

//        Fix fotter problem, or use {force: true} to disable error checking.         
//        cy.contains('Save').click()
//        cy.contains('hours saved.')
    })

    it('change weeks', function() {

        cy.visit(url)
        cy.contains('inTime2')
        cy.contains('Mar 12, 2018')

        cy.get('#next').click()
        cy.contains('Mar 19, 2018')
        cy.get('#next').click()
        cy.contains('Mar 26, 2018')
        cy.get('#prev').click()
        cy.contains('Mar 19, 2018')
    })

    it('sums up first task', function() {

        cy.visit(url)
        cy.contains('Mar 12, 2018')

        cy.get('input[data-test="hourInput"]').first().clear().type('1')
        cy.get('input[data-test="hourInput"]').eq(1).clear().type('2')
        cy.get('input[data-test="hourInput"]').eq(2).clear().type('4')
        cy.get('input[data-test="hourInput"]').eq(3).clear().type('8')
        cy.get('input[data-test="hourInput"]').eq(4).clear().type('3')
        cy.get('input[data-test="hourInput"]').eq(5).clear().type('5')
        cy.get('input[data-test="hourInput"]').eq(6).clear().type('0')

        cy.get('span[data-test="sum"]').first().contains('23')

        cy.get('input[data-test="hourInput"]').first().clear().type('0')
        cy.get('input[data-test="hourInput"]').eq(1).clear().type('0')

        cy.get('span[data-test="sum"]').first().contains('20')
    })

  })