describe('gitlab hosted project documentation.', function() {

    it('visit intime2 home', function() {
        cy.visit('https://spare-time-demos.gitlab.io/intime2/')
        cy.contains('inTime2')

        cy.contains('documentation').click()
        cy.contains('user manual')      
    })

    
  })