= intime2 testcafe e2e tests
man@home

UI integration / end to end tests for inTime2.
tests are written with testcafe  in typescript.

Those test runs will also create screenshots of several pages, to be automatically integrated into documentation (see ../doc)

Tests are multi browser capable, running headless Firefox and Chrome during ci builds.

 * testcafe is installed with: `npm install --save-dev testcafe`
 
* run locally with: 
   ** all tests:       `npm test`
   ** on local machine `npm test -- -T "reporting*" --baseUrl=http://localhost:3000`
   ** subset:          `npm test -- -T "time entry*"` 
   ** run ci like      `npm run test:ci -- --baseUrl=http://intime2-intime2.7e14.starter-us-west-2.openshiftapps.com/`

* test execution during ci build
   ** see `..\..\.gitlab-ci.yml`
   ** running against a staged app version of inTime2 deployed on openshift

* for more details see
   ** testcafe https://devexpress.github.io/testcafe/
   ** test sources: https://gitlab.com/spare-time-demos/intime2/tree/master/qa/e2e/tests
   ** ci test run sample output: https://spare-time-demos.gitlab.io/intime2/reports/e2e/e2e_test_report.html

