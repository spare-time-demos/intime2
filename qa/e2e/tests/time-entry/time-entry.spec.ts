import { Selector } from 'testcafe';
import { baseUrl } from '../baseUrl';

const url: string = baseUrl(process.argv.slice(2));

// tag::e2e[]

fixture `intime2 time-entry page`
  .page(`${url}`);

test('intime2 time entry page is shown with correct title', async t => {

    await t.expect(Selector('h2').innerText)
           .contains('Welcome to inTime2')          // <1>

    await t.takeScreenshot('screenshot_intime2_timeentry'); // <2>
});

test('has hours input fields', async t => {

    await t.expect(Selector('input[data-test=hourInput]').count)
           .gt(6, "at least 7 input fields expected.")
});

// end::e2e[]


test('has message display (green)', async t => {

    await t.expect(Selector('div[data-test=messageDisplay]').exists)
           .ok('has message displayed')

    await t.expect(Selector('div[data-test=messageDisplay]').classNames)
           .contains('alert-success', 'message is green.')        
});

test('has save button', async t => {

    await t.expect(Selector('#save').innerText)
           .match(/Save|Speichern/)

    await t.expect(Selector('#save').tagName)
           .contains('button', 'save should be a real button')

});

test('sum changes on hours change', async t => {

    const sumSelector = Selector('span[data-test=sum]') 

    await t.expect(sumSelector.count)
           .gt(0, 'task sum span found')

    const oldSum = await sumSelector.nth(0).innerText
    
    await t.expect(oldSum).eql('0')

    const firstInput = await Selector('input[data-test=hourInput]').nth(0)

    await t.typeText(firstInput, '12')

    const newSum = await sumSelector.nth(0).innerText

    await t.expect(newSum).eql('12')
});
