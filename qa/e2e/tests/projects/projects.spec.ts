import { Selector } from 'testcafe';
import { baseUrl } from '../baseUrl';

const url: string = baseUrl(process.argv.slice(2));

fixture `intime2 projects page`
.page(`${url}/#/projects`);

test('can navigate to projects page', async t => {

    console.info(`testing, ${url}/#/projects`)

    // await t.debug()

    await t.expect(Selector('body').innerText)
           .match(/Projects|Projekte/)

    await t.takeScreenshot('screenshot_intime2_projects');
});
