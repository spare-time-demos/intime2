import { Selector } from 'testcafe';

fixture `sanity tests: check if project infrastructure is available`
.page `https://gitlab.com`;

test('gitlab git repo accessible on gitlab.com', async t => {
    
    await t.navigateTo('https://gitlab.com/spare-time-demos/intime2');
    await t.takeScreenshot('screenshot_gitlab_repo');
});

test('gitlab hosted intime2 pages are accessible', async t => {
    
    await t.navigateTo('https://spare-time-demos.gitlab.io/intime2/');
    await t.takeScreenshot('screenshot_gitlab_pages');
});
