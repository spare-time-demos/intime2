import * as minimist from 'minimist';

/** 
 * small helper to get baseUrl from --baseUrl command line parameter. 
 * @author man@home
 */
export function baseUrl(argv: string[]) : string {
     
    const args    = minimist(process.argv.slice(2))
    const baseUrl = args.baseUrl ? args.baseUrl : "http://localhost:3000/?lang=en-US"

    console.info(`testing, baseUrl will be: ${baseUrl}`)

    return baseUrl
}