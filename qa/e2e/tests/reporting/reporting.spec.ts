import { Selector } from 'testcafe';
import { baseUrl } from '../baseUrl';

// valid base urls:
// http://intime2-backend-intime2.7e14.starter-us-west-2.openshiftapps.com
// http://localhost:3000

const url: string = baseUrl(process.argv.slice(2));

fixture `intime2 reporting page`
.page(`${url}/#/reporting`);

test('can navigate to reporting page after selecting a week', async t => {

    await t.navigateTo('/#/entry/2018-05-21');

    await t.navigateTo('/#/reporting');

    const body = await Selector('body').innerText;
           
    await t.takeScreenshot('screenshot_intime2_reporting');

    await t.expect(body).contains('\nReports');
    await t.expect(body).contains('testtask 1');
    
});

test('can navigate directly to reporting page', async t => {

    await t.expect(Selector('body').innerText)
           .contains('\nReports')
});
