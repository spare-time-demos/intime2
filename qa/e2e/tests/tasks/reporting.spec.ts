import { Selector } from 'testcafe';
import { baseUrl } from '../baseUrl';

// valid base urls:
// http://intime2-backend-intime2.7e14.starter-us-west-2.openshiftapps.com
// http://localhost:3000

const url: string = baseUrl(process.argv.slice(2));

fixture `intime2 tasks page`
.page(`${url}/#/tasks`);

test('can navigate to tasks page', async t => {

    await t.navigateTo('/#/tasks');

    const body = await Selector('body').innerText;
           
    await t.takeScreenshot('screenshot_intime2_tasks');

    await t.expect(body).contains('\nTasks');
    await t.expect(body).contains('testtask 1');
    
});
