import { Selector } from 'testcafe';

fixture `intime2 backend simple endpoints reachable`
  .page `http://intime2-backend-intime2.7e14.starter-us-west-2.openshiftapps.com/info`;

test('intime2 java backend url /info reachable', async t => {
    await t.navigateTo('/info');

    const bodyTag = Selector('body');
    await t.expect(bodyTag.innerText).contains('hello, this is the inTime2 backend')

    await t.takeScreenshot('screenshot_intime2_info');
});

test('intime2 java backend url /help reachable ', async t => {    
    await t.navigateTo('/help');

    const bodyTag = Selector('body');
    await t.expect(bodyTag.innerText).contains('getting help is overrated')

    await t.takeScreenshot('screenshot_intime2_help');
});
