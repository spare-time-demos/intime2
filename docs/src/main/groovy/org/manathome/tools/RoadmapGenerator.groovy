// RoadmapGenerator.groovy

// retrieve github milestones for given project
// generating intime2-roadmap.adoc file
// @author man@home

package org.manathome.tools

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

println "  RoadmapGenerator: generating roadmap from github: ${new Date().format("YYYY-MM-dd")}"
println "  RoadmapGenerator: milestones"
println "  RoadmapGenerator: token ${this.args[1]}"

def post     = new URL("https://gitlab.com/api/v4/projects/${this.args[0]}/milestones?state=active")

println "  RoadmapGenerator: posting ${post}"

def jsonText = post.getText( requestProperties: ["Private-Token": "${this.args[1]}"])
def json     = new JsonSlurper().parseText(jsonText)

new File(new File("${this.args[2]}").parent).mkdirs()
def roadmapFile = new File("${this.args[2]}")

roadmapFile.write "= Roadmap\n"
roadmapFile.append "man@home\n"
roadmapFile.append  "${new Date().format("YYYY-MM-dd")}\n"
roadmapFile.append  ":icons: font\n\n"

roadmapFile.append  "NOTE: this is a generated.footnote:[generated at ${new Date().format("YYYY-MM-dd HH:mm")} based on gitlab.com intime2 milestones.] roadmap.\n\n"

roadmapFile.append "current plan on gitlab: https://gitlab.com/spare-time-demos/intime2/milestones\n"

def sortedJson = json.sort{
    i1, i2 -> return ("${i1?.title}" <=> "${i2?.title}")}

println JsonOutput.prettyPrint(JsonOutput.toJson(sortedJson))

roadmapFile.append "[plantuml, intime2-roadmap, png]\n"
roadmapFile.append "....\n"
roadmapFile.append "@startgantt\n\n"
roadmapFile.append "Project starts ${new Date().format("YYYY-MM-dd")}\n"
roadmapFile.append "saturday are closed\n"
roadmapFile.append "sunday are closed\n\n"

sortedJson.each { milestone ->
    def days = 5 // arbitrary default..

    if(milestone.start_date != null && milestone.due_date != null) {
        def duration = Date.parse('yyyy-MM-dd', milestone.due_date) - Date.parse('yyyy-MM-dd', milestone.start_date)
        days = duration
    }
    roadmapFile.append  "[${milestone.title}] lasts ${days} days\n";
}

roadmapFile.append  "\n\n"

def previousMilestone = null
sortedJson.each { milestone ->
    if( previousMilestone != null) {
        markupString =   "[${milestone.title}] starts at [${previousMilestone.title}]'s end\n"
        roadmapFile.append  markupString
    }
    previousMilestone = milestone;
}

roadmapFile.append "\n@endgantt\n\n"
roadmapFile.append  "....\n\n"

sortedJson.each { milestone ->
    def timeRange = "^not^ ^planned^ ^yet^"
    if(milestone.start_date != null && milestone.due_date != null) {
        def duration = Date.parse('yyyy-MM-dd', milestone.due_date) - Date.parse('yyyy-MM-dd', milestone.start_date)
        timeRange = "^(${milestone.start_date}^ ^-^ ^${milestone.due_date},^ ^${duration}^ ^days)^"
    }
    markupString =   "* *${milestone.title}* _${milestone.description}_ " +
                     " https://gitlab.com/spare-time-demos/intime2/milestones/${milestone.iid}[=>] " +
                     "${timeRange}\n"

    roadmapFile.append markupString
}

roadmapFile.append  "\n\n"

println "  RoadmapGenerator: done."
