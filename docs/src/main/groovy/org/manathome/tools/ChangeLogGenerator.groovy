// ChangeLogGenerator.groovy

// retrieve github issues for given project
// generating changelog.adoc file

// arg0 milestone
// arg1 project id
// arg2 gitlab access
// arg3 changelog file to write

// @author man@home

package org.manathome.tools

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

println "  ChangeLogGenerator: generating changelog from github: ${new Date().format("YYYY-MM-dd")}"
println "  ChangeLogGenerator: milestone ${this.args[0]}, to ${this.args[3]} "

def post     = new URL("https://gitlab.com/api/v4/projects/${this.args[1]}/issues")
def jsonText = post.getText( requestProperties: ["Private-Token": "${this.args[2]}"])
def json     = new JsonSlurper().parseText(jsonText)

new File(new File("${this.args[3]}").parent).mkdirs()
def changelogFile = new File("${this.args[3]}")

changelogFile.write "= Changelog\n"
changelogFile.append "man@home\n"
changelogFile.append  "${new Date().format("YYYY-MM-dd")}\n"
changelogFile.append  ":icons: font\n\n"
changelogFile.append  "this is a generated changelog file for the https://gitlab.com/spare-time-demos/intime2[inTime2 project].\n\n"

def sortedJson = json.sort{i1, i2 -> return -1 * ("${i1?.milestone?.title ?: "0.0.0.0"}" <=> "${i2?.milestone?.title ?: "0.0.0.0"}")}
println "SORTED ========================================="
println JsonOutput.prettyPrint(JsonOutput.toJson(sortedJson))

def lastMilestone = null
sortedJson.each { issue ->

    if(lastMilestone?.id != issue.milestone?.id ||
       lastMilestone != null && issue.milestone == null) {
        if(issue.milestone == null) {
            changelogFile.append  "\n\n'''"
            changelogFile.append  "\n* _without_ a Milestone\n\n"
        } else {
            changelogFile.append  "\n\n'''"
            milestoneMarkup = "\n* https://gitlab.com/spare-time-demos/intime2/milestones/${issue.milestone?.iid}[*Milestone ${issue.milestone?.title}*] " +
                              "${issue.milestone?.description} " +
                              "${issue.milestone?.due_date}" +
                              "${issue.milestone?.state == "active" ? "icon:tags[role=\"green\"] active" : ""}" +
                              "\n\n"

            changelogFile.append milestoneMarkup
        }
    }
    lastMilestone = issue.milestone

    // link to issue https://gitlab.com/spare-time-demos/intime2/issues/40

    markupString = "\n ** _${issue.title}_, " +
            "icon:tags[role=\"${issue.state == "closed" ? "grey": "red"}\"]" +
            "*${issue.state}*, " +
            "https://gitlab.com/spare-time-demos/intime2/issues/${issue.iid}[issue *${issue.iid}*], " +
            " ^${issue.labels.join(",")?.replaceAll("\\s","")}^ \n"

    changelogFile.append  markupString
}
changelogFile.append  "\n"
changelogFile.append  "\n'''"
changelogFile.append  "\ngenerated at ${new Date().format("YYYY-MM-dd HH:mm")} based on gitlab.com intime2 issues.\n"

println "  ChangeLogGenerator: done."
