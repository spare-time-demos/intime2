= intime2 technical infrastructure slides
2018-05
man@home
:numbered:
:backend: revealjs
:imagesdir: images

== inTime2 technical overview

* kotlin backend with spring boot
* react frontend with typescript and redux
* relational database access with jpa

* a demo project for experimentation

.intime2 time entry page
image::screenshot_intime2_timeentry.png[time entry page]

== key value for this project "long term control"

* *version everything*
* control your *data*(base).
* use your *test efforts wisely*
* ci/cd pipeline for *repeatability* and scaling.


=== version everything

* infrastructure as code (docker, cloud staging)
* build everything through a ci pipeline from scatch
* use diff- and mergeable file formats
* document through
** versioned manual documentation (asciidoc)
** augment docs with tests, annotations, generated reports

[NOTE.speaker]
--
show

* build pipeline: via badges https://gitlab.com/spare-time-demos/intime2
* generated test/doc output: https://spare-time-demos.gitlab.io/intime2/userdoc.html
--

=== control your data(base)

* using liquibase http://liquibase.org for db migractions
* test data fixtures versioned as csv files (excel *can* be useful)
* create test data java constants getting refactorable testdata!
* create database documentation on each build

[NOTE.speaker]
--
show

* chang-log, csv: https://gitlab.com/spare-time-demos/intime2/tree/master/backend/src/main/resources/db/changelog
* generated test/doc output: https://spare-time-demos.gitlab.io/intime2/userdoc.html
* TestData.java fixture constants: https://gitlab.com/spare-time-demos/intime2/blob/master/backend/src/test/java/org/manathome/intime2/generated/TaskTestData.java
--

=== use your *test efforts wisely*

* use unit tests during development - and build your code for testing
* add acceptance and end2end tests sparsly
* use automated tests as documentation, it`s to expensive otherwise
* base your test an stable baselines (no false positives, no fatigue)

[NOTE.speaker]
--
show

* enhance api doc through tests: RESTDoc
* provide screenshots for documentation: testcafe
* let the tests write the spec: jgiven
--


=== ci/cd pipeline for *repeatability* and scaling.

* use build tools (http://gradle.org, npm and gitlab http://gitlab.com
* generate application frontend and backend, database from scratch.
* test them, generate documentation - on each branch!
* deploy it into cloud (staging area)

[NOTE.speaker]
--
* https://gitlab.com/spare-time-demos/intime2/pipelines
--


== best practices&tools

* use gitlab, asciidoctor, gradle
* monitoring, tracking and logging (client&server)
* linter, static checking, sonarcloud.. u.s.w.
* junit5, jgiven, jest, testcafe, jacoco
* swagger, swagger2markup, RESTDoc
* i18n
* charting
* bootstrap

== more detailed infos to read..

https://spare-time-demos.gitlab.io/intime2/manual/intime2-technical-manual.html

.intime2 time entry page
image::screenshot_intime2_timeentry.png[time entry page]
