#
Feature: task creation
  a _project lead_ can add a _task_ with a -name_ and an optional _time range_ in which work may be done for. 

  Scenario: create simple task
    Given a task with name SAMPLE-TASK
    When I save the task
    Then I can find the task again searching by name SAMPLE-TASK
     and I can find the task searching for tasks from 2018-01-11 until 2018-01-30
     and I must not find the task again searching by name OTHER-TASK
  
  Scenario: create task with time range
    Given a task with a name SAMPLE-RANGED-TASK
    When I specify a time range for the task from 2018-01-12 until 2018-03-12
     And I save the task
    Then I can find the task searching for tasks from 2018-01-11 until 2018-13
     And I must not find the task searching for 2018-03-13 until 2018-03-17
  
  Scenario: create task without a name
    Given a task with a name ""
      And a save the task
    Then It will fail 
 