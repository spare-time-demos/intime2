Feature: edit work week
  a _team member_ can add a _work_ hours for open tasks in a given week

  the selected week can be changed.
  all open tasks for the selected week displayed
  each task has an input for each week day accepting 0-10 working hours
  edited working hours can be saved. 

  Scenario: displayed task
    When I select a week 2018-01-01
    Then I can find the all tasks without restriced timereange
     And I can find all tasks with at least one day of their timerange in this week 
     And I must not find any task without a single day of the week in their timerange 
  
  Scenario: tasks with partial time range in week
    Given a selected week 2018-01-01
      And a task with a timerange from 2018-01-03 until 2018-02-15
     Then hour entry for monday and tuesday is disabled
      And hour entry for wednesday and forth is enabled
  