= Diagram Support
man@home
2018-03-23
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:idprefix:

got asciidoctor diagram (plantuml) working for Use Cases, see:

* https://www.calmdevelopment.de/blog/2016/09/jbake-asciidoctor-diagram.html
* http://plantuml.com/

